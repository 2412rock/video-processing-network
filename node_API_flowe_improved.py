import os, paramiko, time
# THIS CODE RUNS IN ABOUT ~ 12 MINUTES
CD_VU_BSC = 'cd /home/template/code/vu-bsc;'


def generate_key_pair():
    os.system("wg genkey | tee privatekey | wg pubkey > publickey")
    priv = open("privatekey", "r")
    pub = open("publickey", "r")
    priv_str = priv.read()
    pub_str = pub.read()
    priv.close()
    pub.close()
    os.system("rm privatekey; rm publickey")
    return priv_str, pub_str


def has_freshly_assigned_file(vm_ip):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    stdin, stdout, stderr = ssh_instance.exec_command(f'{CD_VU_BSC} cat freshly_assigned_process')
    out = stdout.read().decode('ascii').strip("\n")
    ssh_instance.close()
    if 'FRESH_TO_OUTPUT' in out:
        return True
    return False


def start_freshly_assigned_node(vm_ip):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    ssh_instance.exec_command(f'{CD_VU_BSC} python3 freshly_assigned_process.py')
    ssh_instance.close()


def restart_quagga_services(vm_ip):
    node_ssh_instance = get_ssh_instance(vm_ip, '24adna')
    node_ssh_instance.exec_command('systemctl restart ospfd; systemctl restart zebra')
    node_ssh_instance.close()


def set_loopback_address(vm_ip, node_id, lo_flag):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    loopback_addr = '172.16.0.'
    node_id = str(node_id)
    if int(node_id) < 254:
        loopback_addr += node_id
    if lo_flag:
        ssh_instance.exec_command('ip addr add {0}/32 dev lo'.format(loopback_addr))
    ssh_instance.close()
    return loopback_addr


def add_network_to_zebra(ssh_instance, interace_name, address, subnet):
    print(f'(NODE_SETUP) Adding network {address} to zebra..')
    file_name = "zebra" + ".conf"
    f = open(file_name, "a")
    f.write('interface {0}\n ip address {1}/{2}\n!\n'.format(interace_name, address, subnet))
    f.close()
    f = open(file_name, "r")
    lines = f.readlines()
    for line in lines:
        stdin, stdout, stderr = ssh_instance.exec_command(
            'cd /etc/quagga;echo -e {0} >> {1}'.format(line.strip(), file_name))
        std_timeout(stdout)
        # time.sleep(0.5)
    f.close()
    os.system('rm {0}'.format(file_name))


def add_interface_to_ospf(ssh_instance, interface_name):
    print(f'(NODE_SETUP) Adding interface {interface_name} to ospf..')
    file_name = "ospfd" + ".conf"
    f = open(file_name, "a")
    f.write('interface {0}\n!\n'.format(interface_name))
    f.close()
    f = open(file_name, "r")
    lines = f.readlines()
    for line in lines:
        stdin, stdout, stderr = ssh_instance.exec_command(
            'cd /etc/quagga;echo -e {0} >> {1}'.format(line.strip(), file_name))
        std_timeout(stdout)
        # time.sleep(0.5)
    f.close()
    os.system('rm {0}'.format(file_name))


def set_router_id_to_ospf(ssh_instance, router_id):
    print(f'(NODE_SETUP) Setting router id {router_id} for ospf..')
    file_name = "ospfd" + ".conf"
    f = open(file_name, "a")
    f.write('router ospf\n ospf router-id {0}\n!\n'.format(router_id))
    f.close()
    f = open(file_name, "r")
    lines = f.readlines()
    for line in lines:
        stdin, stdout, stderr = ssh_instance.exec_command(
            'cd /etc/quagga;echo -e {0} >> {1}'.format(line.strip(), file_name))
        std_timeout(stdout)
        # time.sleep(0.5)
    f.close()
    os.system('rm {0}'.format(file_name))


def add_network_to_ospf(ssh_instance, address, subnet):
    print(f'(NODE_SETUP) Adding network {address} to ospf..')
    file_name = "ospfd" + ".conf"
    f = open(file_name, "a")
    f.write('router ospf\n network {0}/{1} area 0.0.0.0\n redistribute connected\n!\n'.format(address, subnet))
    f.close()
    f = open(file_name, "r")
    lines = f.readlines()
    for line in lines:
        stdin, stdout, stderr = ssh_instance.exec_command(
            'cd /etc/quagga;echo -e {0} >> {1}'.format(line.strip(), file_name))
        std_timeout(stdout)
        # time.sleep(0.5)
    f.close()
    os.system('rm {0}'.format(file_name))


def configure_wireguard_on_VM(lines, file_name, ssh_instance):
    print(f'(NODE_SETUP) Writing config {file_name} to VM..')
    for line in lines:
        ssh_stdin, ssh_stdout, ssh_stderr = ssh_instance.exec_command(
            'cd /etc/wireguard;echo -e {0} >> {1}'.format(line.strip(), file_name))
        # time.sleep(0.5)
        std_timeout(ssh_stdout)
    ssh_instance.exec_command('systemctl stop ospfd')
    ssh_instance.exec_command('systemctl stop zebra')
    time.sleep(0.5)
    # print('(NODE_SETUP) Bring interface up..')
    ssh_instance.exec_command('wg-quick up {0}'.format(file_name.replace('.conf', '')))
    time.sleep(0.5)
    ssh_instance.exec_command('systemctl start ospfd')
    ssh_instance.exec_command('systemctl start zebra')
    time.sleep(0.5)
    # print('(NODE_SETUP) Enable ip forwarding..')
    ssh_instance.exec_command('sysctl -w net.ipv4.ip_forward=1')
    time.sleep(0.5)


def generate_wireguard_config_client_and_setup(interface_label, privatekey, server_publickey, wg_address, wg_endpoint,
                                               vm_ip):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    print('(NODE_SETUP) Generating client config..')
    file_name = "wg_c_" + str(interface_label) + ".conf"
    f = open(file_name, "a")
    f.write(
        "#Connects to VM2\n[Interface]\nAddress = " + wg_address + "/24\nPrivateKey = " + privatekey +
        "\n[Peer] \n#Peer VM 2\nPublicKey = " + server_publickey + "\nAllowedIPs = 0.0.0.0/0\nEndpoint = " + wg_endpoint)
    f.close()
    f = open(file_name, "r")
    configure_wireguard_on_VM(f.readlines(), file_name, ssh_instance)
    f.close()
    os.system("rm {0}".format(file_name))
    ssh_instance.close()
    return file_name.replace('.conf', '')


def generate_wireguard_config_server_and_setup(interface_label, privatekey, peer_publickey, wg_address, port_number,
                                               vm_ip):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    print('(NODE_SETUP) Generating server config..')
    file_name = "wg_s_" + str(interface_label) + ".conf"
    f = open(file_name, "a")
    f.write(
        "#Connects to VM2\n[Interface]\nAddress = " + wg_address + "/24\nPrivateKey = " +
        privatekey + "\nListenPort = " + port_number + "\n[Peer] \n#Peer VM 2\nPublicKey = " +
        peer_publickey + "\nAllowedIPs = 0.0.0.0/0")
    f.close()
    f = open(file_name, "r")
    configure_wireguard_on_VM(f.readlines(), file_name, ssh_instance)
    f.close()
    os.system("rm {0}".format(file_name))
    ssh_instance.close()
    return file_name.replace('.conf', '')


def get_ssh_instance(vm_ip_address, vm_passwd):
    #print('(NODE_SETUP) Establishing ssh connection with {0}..'.format(vm_ip_address))
    ssh_instance = paramiko.SSHClient()
    # print_debug('SSH CLIENT')
    ssh_instance.load_system_host_keys()
    # print_debug('SSH KEYS')
    ssh_instance.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # print_debug('MISSING HOST KEY')
    ssh_instance.connect(vm_ip_address, username="root", password=vm_passwd, )
    # print('(NODE_SETUP) Connected to ssh')
    return ssh_instance


def reset_wireguard_and_zebra(vm_ip):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    print_debug('Mkdir wireguard')
    ssh_instance.exec_command('cd /etc; rm -rf wireguard; mkdir wireguard')
    print_debug('Rm ospfd and zebra configs')
    ssh_instance.exec_command('cd /etc/quagga; rm ospfd.conf; rm zebra.conf')
    time.sleep(0.5)
    print_debug('Finished')
    ssh_instance.close()


def run_initial_setup(vm_ip):
    print_debug('Running initial setup')
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    print_debug('Executing run.sh')
    ssh_stdin, ssh_stdout, ssh_stderr = ssh_instance.exec_command('./run.sh')
    out = ssh_stdout.read().decode('ascii').strip("\n")
    print_debug('Finished running run.sh')
    ssh_instance.close()


def print_all_q_size(vm_ip):
    print('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX GET CUMULATIVE QUEUE SIZE')
    base_name = 'hello'
    counter = 2
    crt_q_size = get_q_size(vm_ip)  # default q size
    print(
        f'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Q NAME hello elements: {crt_q_size}')

    while True:
        current_q_name = base_name + str(counter)
        crt_q_size = get_q_size(vm_ip, current_q_name)

        if crt_q_size == -1:
            break
        print(
            f'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Q NAME {current_q_name} elements: {crt_q_size}')

        counter += 1

def track_reassign(vm_ip, old_function, new_function, from_q, start_or_stop):
    ssh_instance = get_ssh_instance(vm_ip,'24adna')
    ssh_instance.exec_command(f'{CD_VU_BSC} python3 track_reassign.py {old_function} {new_function} {from_q} {start_or_stop}')
    ssh_instance.close()


def get_cumulative_queue_size_CLI(vm_ip):
    base_name = 'hello'
    counter = 2
    sum = get_q_size_CLI(vm_ip) # default q size
    print(f'[[[[[[[[[[[[[[[[[[[ CUM Q_SIZE {base_name} {sum}')
    while True:
        current_q_name = base_name + str(counter)
        crt_q_size = get_q_size_CLI(vm_ip, current_q_name)
        print(f'[[[[[[[[[[[[[[[[[[[ CUM Q_SIZE {current_q_name} {crt_q_size}')
        if crt_q_size == -1:
            break
        else:
            sum += crt_q_size
            counter += 1

    print(f'[[[[[[[[ SUM {sum}')
    return sum


def get_cumulative_queue_size(vm_ip, ssh_instance=None):
    #print('[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ GET CUMULATIVE QUEUE SIZE')

    ssh_instance_provided = False
    if not ssh_instance:
        ssh_instance_provided = False
        ssh_instance = get_ssh_instance(vm_ip, '24adna')
    else:
        ssh_instance_provided = True
    base_name = 'hello'
    counter = 2
    sum = get_q_size(vm_ip, ssh_instance=ssh_instance) # default q size

    most_used_ss_q_size = sum
    most_used_ss_q_name = 'hello'

    #print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ most_used_q_name {most_used_ss_q_name} -> {most_used_ss_q_size}')
    while True:
        current_q_name = base_name + str(counter)
        crt_q_size = get_q_size(vm_ip, current_q_name, ssh_instance=ssh_instance)
        #print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ crt_q_name {current_q_name} -> {crt_q_size}')
        if crt_q_size > most_used_ss_q_size:
            most_used_ss_q_size = crt_q_size
            most_used_ss_q_name = current_q_name
            #print(f'---------->>>>>>>>>>> UPDATE SS {most_used_ss_q_name} -> {most_used_ss_q_size}')
        #print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ Q NAME {current_q_name} elements: {crt_q_size}')

        if crt_q_size == -1:
            break
        else:
            sum += crt_q_size
            counter += 1
    #print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ SUM {sum}')
    if not ssh_instance_provided:
        ssh_instance.close()

    #print(f'[[[[[[[[[[[[[[[[[ Most used q_name {most_used_ss_q_name} -> {most_used_ss_q_size}')
    if most_used_ss_q_size == 0:
        most_used_ss_q_name = None

    return sum, most_used_ss_q_name, most_used_ss_q_size


def run_pip_installers(vm_ip):
    print_debug('Running pip installer')
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    print_debug('Running pip_inst.sh')
    ssh_stdin, ssh_stdout, ssh_stderr = ssh_instance.exec_command(
        'cd /home/template/code/vu-bsc; chmod +x pip_inst.sh; ./pip_inst.sh')
    out = ssh_stdout.read().decode('ascii').strip("\n")
    print_debug('Finished running')
    ssh_instance.close()


def run_rabbitmq_setup(vm_ip):
    print_debug('Running rabbitmq initial setup')
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    print_debug('Running rabitMQ_inst.sh')
    ssh_stdin, ssh_stdout, ssh_stderr = ssh_instance.exec_command(
        'cd /home/template/code/vu-bsc; chmod +x set_rabbitmq_user.sh; ./set_rabbitmq_user.sh')
    out = ssh_stdout.read().decode('ascii').strip("\n")
    print_debug('Finished running')
    ssh_instance.close()


def update_quagga_config_files(vm_ip, interface_name, node_loopback_addr, node_wireguard_addr):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    print(f'(NODE_SETUP) Start updating quagga files for {node_loopback_addr}..')
    add_interface_to_ospf(ssh_instance, interface_name)
    add_network_to_zebra(ssh_instance, 'lo', node_loopback_addr, '32')
    add_network_to_zebra(ssh_instance, interface_name, node_wireguard_addr, '24')
    add_network_to_ospf(ssh_instance, node_loopback_addr, '32')
    add_network_to_ospf(ssh_instance, node_wireguard_addr, '24')
    ssh_instance.close()


def start_socket(vm_ip, controlller_ip):
    print('(NODE_SETUP) Starting socket')
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    command = f'python3 socket_client.py {controlller_ip}'
    ssh_instance.exec_command(f'cd /home/template/code/vu-bsc; echo "{command}" >> socket_client.txt')
    ssh_instance.close()


def set_id(node_ip, node_id):
    print('(NODE_SETUP) Saving id locally')
    ssh_instance = get_ssh_instance(node_ip, '24adna')
    print('(NODE_SETUP) Removing id.txt..')
    ssh_instance.exec_command(f'cd /home/template/code/vu-bsc; rm id.txt')
    time.sleep(0.5)
    print('(NODE_SETUP) Writing id.txt..')
    ssh_instance.exec_command(f'cd /home/template/code/vu-bsc; echo {node_id} >> id.txt')
    print_debug('Finished command')
    ssh_instance.close()


def print_debug(msg):
    print(f'(NODE_SETUP) {msg}')


def std_timeout(std):
    # print_debug('STD timeout check running')
    endtime = time.time() + 5
    while not std.channel.eof_received:
        # time.sleep(1)
        if time.time() > endtime:
            std.channel.close()
            print_debug('STD timed out')
            break


def stop_ospfd_and_zebra(vm_ip):
    print_debug('STOP OSPFD AND ZEBRA')
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    ssh_instance.exec_command('systemctl stop ospfd; systemctl stop zebra')
    time.sleep(0.5)
    ssh_instance.close()
    # print_debug('---------- STOP DONE')


def start_ospfd_and_zebra(vm_ip):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    ssh_instance.exec_command('systemctl start ospfd; systemctl start zebra')
    time.sleep(1)
    ssh_instance.close()


def bring_down_all_wireguard_interfaces(vm_ip, connection_array):
    print('BRING DOWN ALL INTERFACES ')
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    # ssh_instance.exec_command('systemctl stop ospfd; systemctl stop zebra')
    time.sleep(0.5)

    ssh_instance.exec_command('cd /home/template/code/vu-bsc; python3 bring_down_interfaces.py >> kek2.txt 2>&1')
    ssh_instance.close()
    # print_debug('BRING DOWN ALL INTERFACE SUCCESFUL')


def bring_up_all_wireguard_interfaces(vm_ip, connection_array):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    # ssh_instance.exec_command('systemctl stop ospfd; systemctl stop zebra')
    # time.sleep(0.5)

    for connection in connection_array:
        # print_debug('FOR LOOOOP')
        interface_name = connection.interface_name
        interface_name = interface_name.replace('.conf', '')
        # print_debug(f' BRING UP INTERFACE {interface_name}')
        ssh_stdin, ssh_stdout, ssh_stderr = ssh_instance.exec_command(f'wg-quick up {interface_name}')
        std_timeout(ssh_stdout)
        # time.sleep(0.5)
        # interface_name = interface_name + '.conf'
        # print_debug(f' DELETE INTERFACE {interface_name}')
        # ssh_instance.exec_command(f'cd /etc/wireguard; rm {interface_name}')
        # print_debug(f'BRING UP INTERFACE {interface_name} DONE ')
        # time.sleep(0.5)
    ssh_instance.close()
    # print_debug('BRING UP ALL INTERFACE SUCCESFUL')


def wg_down(vm_ip, interface_name):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    print_debug(f'WG DOWN {interface_name}')
    ssh_instance.exec_command(f'wg-quick down {interface_name}')
    time.sleep(1)
    ssh_instance.close()


def launch_checker_daemon(vm_ip):
    print(f'Launch checker daemon {vm_ip}')
    if not check_daemon_runs(vm_ip):
        print_debug('RUN CHECKER DAEMON')
        ssh_instance = get_ssh_instance(vm_ip, '24adna')
        ssh_instance.exec_command(f'cd /home/template/code/vu-bsc; python3 checker_daemon.py')
        time.sleep(1)
        ssh_instance.close()


def wg_up(vm_ip, interface_name):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    print_debug(f'WG UP {interface_name}')
    ssh_instance.exec_command(f'wg-quick up {interface_name}')
    time.sleep(1)
    ssh_instance.close()


def delete_checker_daemon_config_line(vm_ip, line=None, id=None, file_to_delete='consumer.txt'):
    print('DELETE CHECKER DAEMON CONFIG FILES')
    ssh_instance = get_ssh_instance(vm_ip, '24adna')

    if line:
        print(f'DELETE LINEEEEEEE {line} for node {id}')
        ssh_instance.exec_command(f'cd /home/template/code/vu-bsc; python3 delete_checker_config_line.py {line}')
    else:
        print('DELETEE ENTIRE FILEE')
        ssh_instance.exec_command(f'cd /home/template/code/vu-bsc;  rm {file_to_delete}')
    time.sleep(1)
    ssh_instance.close()


def delete_wireguard_interfaces(vm_ip, connection_array, vm_id):
    print_debug('----------------------------------------------------DELEEETE WIREGUARD INTERFACES()')

    ssh_instance = get_ssh_instance(vm_ip, '24adna')

    # ssh_instance.exec_command('systemctl stop ospfd; systemctl stop zebra')
    # time.sleep(1)

    for connection in connection_array:
        # print_debug('FOR LOOOOP')
        interface_name = connection.interface_name
        interface_name = interface_name.replace('.conf', '')
        interface_name_client = interface_name.replace('wg_s', 'wg_c')
        interface_name_server = interface_name.replace('wg_c', 'wg_s')
        # print_debug(f'DELETE INTERFACE rm {interface_name_client}.conf; rm {interface_name_server}.conf')
        # wg_down(vm_ip, interface_name_client)
        # wg_down(vm_ip, interface_name_server)

        stdin, stdout, stderr = ssh_instance.exec_command(
            f'rm /etc/wireguard/{interface_name_client}.conf; rm /etc/wireguard/{interface_name_server}.conf; cd ~')
        # print_debug('-------------EXEC COMMAND SUCCESFUL')

        std_timeout(stdout)
        std_timeout(stderr)
        out = stdout.read().decode('ascii').strip("\n")
        err = stderr.read().decode('ascii').strip("\n")
        # interface_name = interface_name + '.conf'
        # print_debug(f' DELETE INTERFACE {interface_name}')
        # ssh_instance.exec_command(f'cd /etc/wireguard; rm {interface_name}')
        # print_debug(f'DELETE INTERFACE {interface_name} DONE-> {out} ERR-> {err}')

        # ssh_instance.exec_command('systemctl start ospfd; systemctl start zebra')
    ssh_instance.close()
    # print_debug('DELETE DONE')


def create_next_producer_folder(vm_ip, folder_name):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    #print_debug(f'Creating next producer folder for {vm_ip} with name {folder_name}')
    ssh_instance.exec_command(f'cd /home/template/code/vu-bsc; mkdir {folder_name}')
    ssh_instance.close()


def setup_consumer(queue_ip, vm_ip, task_type, third_argument=None, forth_argument=None, node_id=None):
    redirect_out_name = queue_ip.replace('.', '_') + '.txt'
    print(
        f'(NODE_SETUP) ========= {vm_ip} Node_id {node_id} SETUP CONSUMER cd /home/template/code/vu-bsc; python3 consumer_flower_improved.py {task_type} {queue_ip} {third_argument} {forth_argument}')
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    command = f'python3 consumer_flower_improved.py {task_type} {queue_ip}'
    #print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[  SETUP CONSUMER {command}')
    if third_argument:
        command += f' {third_argument}'  # >> consumer.py.log 2>&1'
        #print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[  THIRD ARUGMENT ADD {command}')
    if forth_argument:
        #print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[  FORTH ARUMENT ADD {command}')
        command += f' {forth_argument}'

    ssh_stdin, ssh_stdout, ssh_stderr = ssh_instance.exec_command(
        f'cd /home/template/code/vu-bsc; echo "{command}" >> consumer.txt')
    #print_debug('WROOOOOOOOOOOOOOTEEEEEEEEEEEEEE COMAAAAAAAAND ' + command)
    #time.sleep(10)
    ssh_instance.close()


def setup_producer(vm_ip, loopback_addr, queue_name=None, folder_name=None):
    print_debug(f'======================= f{vm_ip} Setting up producer {loopback_addr}')
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    command = ''
    if queue_name and folder_name:
        command = f'python3 producer.py {loopback_addr} {queue_name} {folder_name}'  # >> producer.py.log 2>&1'
        #print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ SETUP_PRODUCER QUEUE NAME AND FOLDER NAME PROVIDED')
    else:
        command = f'python3 producer.py {loopback_addr}'
    #print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[  SETUP PRODUER COMMAND {command}')
    ssh_stdin, ssh_stdout, ssh_stderr = ssh_instance.exec_command(
        f'cd /home/template/code/vu-bsc; echo "{command}" >> producer.txt ')

    ssh_instance.close()


def get_q_size_CLI(vm_ip, q_name=None):
    #print_debug(f'GETTING Q SIZE OF NODE {vm_ip}')
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    command = 'cd /home/template/code/vu-bsc; python3 print_q_size.py'
    if q_name:
        command += f' {q_name}'
    ssh_stdin, ssh_stdout, ssh_stderr = ssh_instance.exec_command(command)
    time.sleep(0.5)
    out = ssh_stdout.read().decode('ascii').strip("\n")
    # print_debug(f'Q_SIZE {out} of node {vm_ip}')
    ssh_instance.close()
    return int(out)


def get_q_size(vm_ip, q_name=None, ssh_instance=None):
    #print_debug(f'GETTING Q SIZE OF NODE {vm_ip}')
    ssh_instance_provided = False
    if not ssh_instance:
        ssh_instance = get_ssh_instance(vm_ip, '24adna')
        ssh_instance_provided = False
    else:
        ssh_instance_provided = True
    if not q_name:
        q_name = 'hello'
    command = f'cd /home/template/code/vu-bsc; cat pika_q_size_{q_name}.txt'

    ssh_stdin, ssh_stdout, ssh_stderr = ssh_instance.exec_command(command)
    time.sleep(0.5)
    out = ssh_stdout.read().decode('ascii').strip("\n")
    # print_debug(f'Q_SIZE {out} of node {vm_ip}')
    if not ssh_instance_provided:
        ssh_instance.close()
    q_size = -1
    try:
        q_size = int(out)
    except:
        pass
    #print(f'Get q_size for q {q_name} is {q_size}')
    return q_size



def kill_ffmpeg(vm_ip):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    os.system('cd /home/template/code/vu-bsc; rm *.mp4')
    time.sleep(0.5)
    ssh_instance.close()


def write_timestamp(ssh_instance):
    ssh_instance.exec_command(f'{CD_VU_BSC} python3 write_timestamp.py')


def check_daemon_runs(vm_ip):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    ssh_stdin, ssh_stdout, ssh_stderr = ssh_instance.exec_command('ps -fA | grep python3')
    time.sleep(0.5)
    out = ssh_stdout.read().decode('ascii').strip("\n")
    if 'checker_daemon.py' in out:
        # print_debug('CHECKER IS RUNNING ')
        return True
    # print_debug('CHECKER NOT RUNNING')
    return False


def launch_ffmpeg_last_run_check(vm_ip):
    print_debug('Launch ffmpeg last run check')
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    # prevent from running dupplicates, this methid is called from controller assign node
    # an idle node can have this process running already
    ssh_stdin, ssh_stdout, ssh_stderr = ssh_instance.exec_command('ps -fA | grep python3')
    time.sleep(0.5)
    out = ssh_stdout.read().decode('ascii').strip("\n")
    if 'muie' not in out:
        print_debug('HAS NOT BEEN LAUNCHED')
        ssh_instance.exec_command(f'cd /home/template/code/vu-bsc; python3 muie.py')
    print_debug('Done')
    ssh_instance.close()


def ffmpeg_has_not_ran_in_x_seconds(vm_ip, seconds, ssh_instance=None):

    ssh_instance_provided = False
    if not ssh_instance:
        ssh_instance_provided = False
        ssh_instance = get_ssh_instance(vm_ip, '24adna')
    else:
        ssh_instance_provided = True
    ssh_stdin, ssh_stdout, ssh_stderr = ssh_instance.exec_command(
        f'cd /home/template/code/vu-bsc; python3 ffmpeg_has_not_ran_in_x_seconds.py {seconds}')
    out = ssh_stdout.read().decode('ascii').strip("\n")
    err = ssh_stderr.read().decode('ascii').strip("\n")
    #print_debug(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ FMMPEG HAS NOT RUN RESPONSE |{out}| |{err}|')
    if not ssh_instance_provided:
        ssh_instance.close()
    if out == 'True':
        return True
    return False


def check_ffmpeg_runs(vm_ip):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    ssh_stdin, ssh_stdout, ssh_stderr = ssh_instance.exec_command('ps -fA | grep ffmpeg')
    time.sleep(0.5)
    out = ssh_stdout.read().decode('ascii').strip("\n")
    if '-i part' in out:
        #print_debug(
           # '??????????????????????????????????????????????????????????????????????????????????????????????????? FFMPEG POSITIVE RUNNING ')
        print('*********************** FFMPEG RUNS TRUE')
        return True
    #print_debug(
        #'???????????????????????????????????????????????????????????????????????????????????????????????????????? FFMPEG NEGATIVE RUNNING')
    print('*********************** FFMPEG RUNS FALSE')
    return False


def check_sconsumer_process_running(vm_ip):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    ssh_stdin, ssh_stdout, ssh_stderr = ssh_instance.exec_command('ps -fA | grep "python3 consumer_flower_improved.py"')
    time.sleep(0.5)
    out = ssh_stdout.read().decode('ascii').strip("\n")
    if '172.16.0' in out:
        # print_debug('CONSUMER IS RUNNING ')
        return True
    # print_debug('CONSUMER NOT RUNNING')
    return False


def run_merge_parts_service(vm_ip):
    print(f'Run merge parts service {vm_ip}')
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    command = 'python3 merge_parts.py'
    ssh_instance.exec_command(f'cd /home/template/code/vu-bsc; echo "{command}" >> merge_parts.txt')
    time.sleep(0.5)
    ssh_instance.close()


def downloaded_mp4(vm_ip):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    stdin, stdout, stderr = ssh_instance.exec_command(f'{CD_VU_BSC} ls -l *.mp4')

    result = False
    try:
        out = stdout.read().decode('ascii').strip("\n")
        if '.mp4' in out:
            print(f'^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ {vm_ip} DOWNLOADED_MP3 OUTPUT {out} RETURN TRUE')
            result = True
        else:
            # Output empty
            print(f'^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ {vm_ip} DOWNLOADED_MP3 OUTPUT {out} RETURN FALSE')
    except:
        print(f'^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ {vm_ip} CANNOT READ OUTPUT RETURN FALSE')
    return result

def delete_leftover_mp4(vm_ip):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    ssh_instance.exec_command(f'{CD_VU_BSC} rm *.mp4')
    ssh_instance.close()

def reset_rabbitmq(vm_ip):
    print_debug(f'Resetting rabbitmq for node {vm_ip}')
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    stdin, stdout, stderr = ssh_instance.exec_command(f'{CD_VU_BSC} chmod +x reset_rabbitMQ.sh; ./reset_rabbitMQ.sh')
    out = stdout.read().decode('ascii').strip("\n")
    ssh_instance.close()
    print_debug('Reset rabbitmq success')

def delete_all_self_consume_producer_folders(vm_ip):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    ssh_instance.exec_command(f'{CD_VU_BSC} rm -rf producer_folder_self_*')
    ssh_instance.close()

def add_FIN_file(vm_ip):
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    ssh_instance.exec_command('cd /home/template/code/vu-bsc/producer_folder; touch END_OF_CONN')
    time.sleep(0.5)
    ssh_instance.close()


def gracefully_stop_consuming(vm_ip):
    print_debug('Gracefully stop consuming')
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    stdin, stdout, stderr = ssh_instance.exec_command(
        'cd /home/template/code/vu-bsc; touch stop_further_processing')  # >> kill_process.py.log 2>&1')

    time.sleep(1)
    ssh_instance.close()


def kill_process(vm_ip, process_name):
    print_debug(f'---> KILL PROCESS {process_name}')
    ssh_instance = get_ssh_instance(vm_ip, '24adna')
    stdin, stdout, stderr = ssh_instance.exec_command(
        f'cd /home/template/code/vu-bsc; python3 kill_process.py {process_name}')  # >> kill_process.py.log 2>&1')

    std_timeout(stdout)
    std_timeout(stderr)
    out = stdout.read().decode('ascii').strip("\n")
    err = stderr.read().decode('ascii').strip("\n")
    time.sleep(0.5)
    ssh_instance.close()


def connect_nodes(producer_ip, consumer_ip, producer_id, consumer_id, port_producer, vm_passwd, subnet, lo_flag=True):
    print_debug(f'============= CONNECTING CONSUMER {consumer_id} to PRODUCER {producer_id} TO ')
    private_key_consumer, public_key_consumer = generate_key_pair()
    private_key_producer, public_key_producer = generate_key_pair()

    producer_wg_addr = subnet + '1'
    consumer_wg_addr = subnet + '2'

    producer_interface_with_consumer = generate_wireguard_config_server_and_setup(subnet.replace('.', '_'),
                                                                                  private_key_producer,
                                                                                  public_key_consumer,
                                                                                  producer_wg_addr, port_producer,
                                                                                  producer_ip)

    consumer_interface_with_producer = generate_wireguard_config_client_and_setup(subnet.replace(".", "_"),
                                                                                  private_key_consumer,
                                                                                  public_key_producer,
                                                                                  consumer_wg_addr,
                                                                                  producer_ip + ":" + port_producer,
                                                                                  consumer_ip)

    producer_loopback_addr = set_loopback_address(producer_ip, producer_id, lo_flag)
    consumer_loopback_addr = set_loopback_address(consumer_ip, consumer_id, lo_flag)

    update_quagga_config_files(producer_ip, producer_interface_with_consumer, producer_loopback_addr, producer_wg_addr)
    update_quagga_config_files(consumer_ip, consumer_interface_with_producer, consumer_loopback_addr, consumer_wg_addr)

    restart_quagga_services(producer_ip)
    restart_quagga_services(consumer_ip)

    producer_shh = get_ssh_instance(producer_ip, vm_passwd)
    consumer_ssh = get_ssh_instance(consumer_ip, vm_passwd)
    producer_shh.exec_command('ping -n 30 {0}'.format(consumer_wg_addr))
    time.sleep(0.5)
    consumer_ssh.exec_command('ping -n 30 {0}'.format(producer_wg_addr))
    time.sleep(0.5)

    producer_shh.close()
    consumer_ssh.close()
    return (producer_interface_with_consumer + '.conf'), (consumer_interface_with_producer + '.conf')
