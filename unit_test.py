import node_API, VMware_API, time
from datetime import datetime

start_time_total = time.time()

VM_PASS = '24adna'
FINAL_FOLDER_ELEMENTS = 8

node_1_ip = VMware_API.turn_on_vm_and_get_ip('1')
node_2_ip = VMware_API.turn_on_vm_and_get_ip('2')
node_6_ip = VMware_API.turn_on_vm_and_get_ip('6')

node_1_ssh = node_API.get_ssh_instance(node_1_ip, VM_PASS)
node_2_ssh = node_API.get_ssh_instance(node_2_ip, VM_PASS)
node_6_ssh = node_API.get_ssh_instance(node_6_ip, VM_PASS)


def run_test(node_ssh, test_nr):
    print(f'--------------------------- RUNNING TEST {test_nr} ---------------------------')

    ssh_stdin, ssh_stdout, ssh_stderr = node_ssh.exec_command('./move.sh')
    da = ssh_stdout.read().decode('ascii').strip("\n")
    current_count = -1

    while True:
        ssh_stdin, ssh_stdout, ssh_stderr = node_6_ssh.exec_command(
            'cd /home/template/code/vu-bsc/final_folder; ls | wc -l')
        final_folder_count = ssh_stdout.read().decode('ascii').strip("\n")
        #print(f'Final folder count: {final_folder_count}')
        final_folder_count = int(final_folder_count)

        if final_folder_count == FINAL_FOLDER_ELEMENTS:
            print(f'PASSED TEST {test_nr}')
            ssh_stdin, ssh_stdout, ssh_stderr = node_6_ssh.exec_command(
                f'cd /home/template/code/vu-bsc; mkdir folder_{test_nr}; cp -r final_folder folder_{test_nr}')
            node_API.std_timeout(ssh_stdout)
            time.sleep(8)
            #ssh_stdin, ssh_stdout, ssh_stderr = node_6_ssh.exec_command(
              #  'cd /home/template/code/vu-bsc/final_folder; ls')
            #ssh_stdout.read().decode('ascii').strip("\n")
            #out = ssh_stdout.read().decode('ascii').strip("\n")
            #print(f'FINAL FOLDER OUTPUT: \n {out}')

            ssh_stdin, ssh_stdout, ssh_stderr = node_6_ssh.exec_command(
                'cd /home/template/code/vu-bsc/final_folder; rm *')
            node_API.std_timeout(ssh_stdout)
            #print('=============================================================')
            return
        else:
            if final_folder_count > current_count:
                current_count = final_folder_count
                print(f'COUNT HAS INCREASED {current_count}')

            time.sleep(5)


#start_time = time.time()
#run_test(node_1_ssh, '1')
#print("--- %s minutes ---" % ((time.time() - start_time)/60))
#print('=============================================================')

start_time = time.time()
run_test(node_2_ssh, '2')
print("--- %s minutes ---" % ((time.time() - start_time)/60))
print('=============================================================')

#start_time = time.time()
#run_test(node_1_ssh, '3')
#print("--- %s minutes ---" % ((time.time() - start_time)/60))
#print('=============================================================')

node_1_ssh.close()
node_2_ssh.close()
node_6_ssh.close()

now = datetime.now()
dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
print(f'ALL TESTS HAVE PASSED')
print("--- %s minutes ---" % ((time.time() - start_time_total)/60))
