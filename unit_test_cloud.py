import node_API, VMware_API, time, sys
from datetime import datetime

start_time_total = time.time()

VM_PASS = '24adna'
FINAL_FOLDER_ELEMENTS = int(sys.argv[1])

node_1_ip = '10.149.21.1'#VMware_API.turn_on_vm_and_get_ip('1') First pipeline node
merge_parts_node_ip = '10.149.21.4'#VMware_API.turn_on_vm_and_get_ip('6')

node_1_ssh = node_API.get_ssh_instance(node_1_ip, VM_PASS)
node_6_ssh = node_API.get_ssh_instance(merge_parts_node_ip, VM_PASS)


def run_test(node_ssh, test_nr):
    print(f'--------------------------- RUNNING TEST {test_nr} ---------------------------')

    ssh_stdin, ssh_stdout, ssh_stderr = node_ssh.exec_command('cd /home/template/code; cp -r VU-BSC-VIDEO-PARTS/* vu-bsc/producer_folder')
    da = ssh_stdout.read().decode('ascii').strip("\n")
    current_count = -1

    while True:
        ssh_stdin, ssh_stdout, ssh_stderr = node_6_ssh.exec_command(
            'cd /home/template/code/vu-bsc/final_folder; ls | wc -l')
        final_folder_count = ssh_stdout.read().decode('ascii').strip("\n")
        final_folder_count = int(final_folder_count)

        if final_folder_count == FINAL_FOLDER_ELEMENTS:
            print(f'PASSED TEST {test_nr}')
            return
        else:
            if final_folder_count > current_count:
                current_count = final_folder_count
                print(f'COUNT HAS INCREASED {current_count}')
            time.sleep(1)


start_time = time.time()
run_test(node_1_ssh, '1')
print("--- %s minutes ---" % ((time.time() - start_time)/60))
print('=============================================================')

node_1_ssh.close()
node_6_ssh.close()

print(f'ALL TESTS HAVE PASSED')
print("--- %s minutes ---" % ((time.time() - start_time_total)/60))
