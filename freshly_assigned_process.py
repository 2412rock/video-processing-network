import os, time

FILE_NAME = 'freshly_assigned_file'
WRITE_TO_FILE = 'FRESH_TO_OUTPUT'
SLEEP_TIME = 20

if __name__ == '__main__':
    f = open(FILE_NAME, 'w')
    f.write(WRITE_TO_FILE)
    f.close()
    time.sleep(SLEEP_TIME)
    os.remove(FILE_NAME)
