import datetime, sys

if __name__ == '__main__':
    input_seconds = sys.argv[1]
    input_seconds = float(input_seconds)

    f = open('last_ffmpeg_run', 'r')
    time_from_file = f.read()
    f.close()

    time_from_file_obj = datetime.datetime.strptime(time_from_file, '%Y-%m-%d %H:%M:%S')
    time_diff = datetime.datetime.now() - time_from_file_obj
    time_diff = time_diff.seconds
    if time_diff >= input_seconds:
        print('True')
    else:
        print('False')
