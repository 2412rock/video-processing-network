import os, natsort
import time


def has_all_parts():
    files = os.listdir('producer_folder')
    file_names = []
    for file in files:
        # build file_name
        #print(f'current file {file}')
        char_index = 0
        startFilename = 'startFilename'
        filename = None
        for char in file:
            if 'startFilename' not in file[char_index:]:
                filename = file[char_index + (len(startFilename) - 1):]
                file_names.append(filename)
                break
            char_index += 1
        first_half_of_filename = file[:char_index]

        numbers_extracted_from_filename = [int(s) for s in first_half_of_filename.split('-') if s.isdigit()]
        #print(f'numbers {numbers_extracted_from_filename}')
        count_part = 0
        total_parts = numbers_extracted_from_filename[1]

        for file in files:
            if filename in file:
                # detected a part
                count_part += 1
        if total_parts == count_part:
            #print(f'ALL PARTS HAVE BEEN RECEIVED for file {filename}')
            text_file = create_parts_txt_file(filename)
            ffmpeg_merge_parts(text_file, f'MERGED_{filename}')
            delete_files(filename)
            break


def create_parts_txt_file(complete_filename):
    files = os.listdir('producer_folder')
    file_names = []
    for file in files:
        if '.git' not in file and complete_filename in file:
            file_names.append(file)
    #print(file_names)
    #print('-------')
    file_names = natsort.natsorted(file_names)
    #print(file_names)
    text_file_name = complete_filename + '.txt'
    #print(f'Writing text file {text_file_name} ')
    f = open(f'producer_folder/{text_file_name}', 'w')
    for file in file_names:
        f.write(f"file '{file}'")
        f.write('\n')
    return text_file_name


def ffmpeg_merge_parts(text_file, output_name):
    os.system(f'cd producer_folder; ffmpeg -f concat -safe 0 -i "{text_file}" -c copy {output_name}')
    os.system(f'cd producer_folder; mv {output_name} ../final_folder/')
    os.system(f'cd producer_folder; mv {text_file} ../final_folder/')


def delete_files(filename):
    files = os.listdir('producer_folder')
    for file in files:
        if filename in file:
            os.remove(f'producer_folder/{file}')


if __name__ == '__main__':
    while True:
        #print('checking for complete files')
        has_all_parts()
        time.sleep(3)
