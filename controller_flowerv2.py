import datetime
import time
import json, sys, time
# THIS CODE RUNS IN ABOUT ~ 12 MINUTES
import get_queue_size
import node_API, VMware_API, _thread, threading, connection, node, filter_types, VMware_API
import socket
import node_API, VMware_API, socket, _thread, threading, connection, node, filter_types, VMware_API
import node_API, VMware_API, _thread, threading, connection, node, filter_types, VMware_API
import unit_test_cloud_flower_pipeline

test_var = 0
nodes = []
mutex = threading.Lock()
mutex_self_scale = threading.Lock()
mutex_am_busiest = threading.Lock()
freshly_scaled_nodes = {}

SCALE_UP_THRESHOLD_VALUE = 18  # amount of seconds left until work is done
MINIMUM_MOST_BUSY_THRESHOLD = 20
SPLIT_NODE_LO = '172.16.0.1'
BLUR_NODE_LO = '172.16.0.2'
STABILIZE_NODE_LO = '172.16.0.3'
ADD_WATERMARK_NODE_LO = '172.16.0.4'
REVERSE_NODE_LO = '172.16.0.5'
VINTAGE_NODE_LO = '172.16.0.6'
MERGE_PARTS_NODE_LO = '172.16.0.7'

last_used_port = 51820
subnet = 20

FRESHLY_ASSINED_NODE_SKIP_COUNT = 14
SLEEP_BETWEEN_CHECKS = 10
FRESHLY_SCALED_NODES_SKIP = 0
PENULTIM_PIPELINE_NODE = '6'
PENULTIM_PIPELINE_FUNCTION = filter_types.VINTAGE
PIPELINE_NODES = []
LAST_PIPELINE_NODE = '7'

pipeline_nodes = ['1', '2', '3', '4', '5', '6', '7']
available_scale_up_nodes = []

MAX_NR_CONSUMERS = 4
LAST_SCALE_UP = datetime.datetime.now()
NO_SCALEUP_THRESHOLD = 15


def get_port():
    global last_used_port
    last_used_port += 1
    return last_used_port


def get_subnet():
    global subnet
    subnet += 1
    return f'10.0.{subnet}.'


def set_last_used_port(node_id, port):
    get_node(node_id).las_used_port = port


def get_node_last_used_port(node_id):
    return get_node(node_id).las_used_port


def get_node_ip(node_id):
    return get_node(node_id).ip


def get_node_function(node_id):
    return get_node(node_id).function


def get_idle_node():
    for node in nodes:
        if node.function == 'IDLE':
            return node
    return None


def add_node(node):
    nodes.append(node)


def get_node(id):
    for node in nodes:
        if node.id == id:
            return node


def get_subnets_of_node(id):
    # TO DO
    # returns all subnets for now because I suspect a network conflict
    subnets = []
    for node in nodes:
        connections = node.connections
        for connection in connections:
            # print(f' (iiiiiiiiiiiiiiiiiiiiii) subnet of node id {id} {connection.subnet}')
            subnets.append(connection.subnet)

    return subnets


def get_next_subnet(subnets):
    digit = '1'
    current = f'10.0.{digit}.x'

    while current in subnets:
        digit = int(digit) + 1
        digit = str(digit)
        current = f'10.0.{digit}.x'
    return current


def get_subsequent_function(current_func):
    if current_func == filter_types.SPLIT:
        return filter_types.BLUR
    elif current_func == filter_types.BLUR:
        return filter_types.STABILIZE
    elif current_func == filter_types.STABILIZE:
        return filter_types.ADD_WATERMARK
    elif current_func == filter_types.ADD_WATERMARK:
        return filter_types.REVERSE
    elif current_func == filter_types.REVERSE:
        return filter_types.VINTAGE
    elif current_func == filter_types.VINTAGE:
        return filter_types.MERGE_PARTS
    elif current_func == filter_types.MERGE_PARTS:
        return filter_types.MERGE_PARTS


def get_subsequent_nodes_of(node_function, id, func):
    sub_fun = get_subsequent_function(node_function)
    result = []
    if func == filter_types.MERGE_PARTS:
        # If there are multiple end of the line nodes, only send to the static one
        # print('********************* RETURNING ONLY NODE 6')
        result.append(get_node(LAST_PIPELINE_NODE))
        return result

    if sub_fun is not None:
        for node in nodes:
            if node.function == sub_fun and node.id != id:  # prevent connecting to itself
                result.append(node)
    return result


def reset_node_and_neighbours_connection(node_id):
    node = get_node(node_id)
    for connection in node.connections:
        w_id = connection.with_node_id
        w_node = get_node(w_id)
        connection_counter = -1
        for w_node_conn in w_node.connections:
            connection_counter += 1
            if w_node_conn.with_node_id == node_id:
                break

        w_node.connections.pop(connection_counter)


def init_idle_node(vm_ip):
    node_API.run_initial_setup(vm_ip)
    node_API.run_pip_installers(vm_ip)
    node_API.run_rabbitmq_setup(vm_ip)
    node_API.reset_wireguard_and_zebra(vm_ip)


def boot_and_init_idle_nodes():
    print(
        '-------------------------------------------------------------------------------------------------------------> Initialising idle nodes')
    # setup_node_ip = VMware_API.turn_on_vm_and_get_ip('setup_vm')
    # node_API.reset_wireguard_and_zebra(setup_node_ip)
    threads = []

    idle_node_1_ip = '10.149.21.7'  # VMware_API.turn_on_vm_and_get_ip('10')
    # node_API.run_initial_setup(idle_node_1_ip)
    # node_API.run_pip_installers(idle_node_1_ip)
    # node_API.run_rabbitmq_setup(idle_node_1_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_1_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_1_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_2_ip = '10.149.21.8'  # VMware_API.turn_on_vm_and_get_ip('11')
    # node_API.run_initial_setup(idle_node_2_ip)
    # node_API.run_pip_installers(idle_node_2_ip)
    # node_API.run_rabbitmq_setup(idle_node_2_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_2_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_2_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_3_ip = '10.149.21.9'  # VMware_API.turn_on_vm_and_get_ip('12')
    # node_API.run_initial_setup(idle_node_3_ip)
    # node_API.run_pip_installers(idle_node_3_ip)
    # node_API.run_rabbitmq_setup(idle_node_3_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_3_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_3_ip,))
    processThread.start()
    threads.append(processThread)
    #
    idle_node_4_ip = '10.149.21.10'  # VMware_API.turn_on_vm_and_get_ip('13')
    # node_API.run_initial_setup(idle_node_4_ip)
    # node_API.run_pip_installers(idle_node_4_ip)
    # node_API.run_rabbitmq_setup(idle_node_4_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_4_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_4_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_5_ip = '10.149.21.11'  # VMware_API.turn_on_vm_and_get_ip('14')
    # node_API.run_initial_setup(idle_node_5_ip)
    # node_API.run_pip_installers(idle_node_5_ip)
    # node_API.run_rabbitmq_setup(idle_node_5_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_5_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_5_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_6_ip = '10.149.21.12'  # VMware_API.turn_on_vm_and_get_ip('15')
    # node_API.run_initial_setup(idle_node_6_ip)
    # node_API.run_pip_installers(idle_node_6_ip)
    # node_API.run_rabbitmq_setup(idle_node_6_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_6_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_6_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_7_ip = '10.149.21.13'  # VMware_API.turn_on_vm_and_get_ip('16')
    # node_API.run_initial_setup(idle_node_7_ip)
    # node_API.run_pip_installers(idle_node_7_ip)
    # node_API.run_rabbitmq_setup(idle_node_7_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_7_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_7_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_8_ip = '10.149.21.14'  # VMware_API.turn_on_vm_and_get_ip('17')
    # node_API.run_initial_setup(idle_node_8_ip)
    # node_API.run_pip_installers(idle_node_8_ip)
    # node_API.run_rabbitmq_setup(idle_node_8_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_8_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_8_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_9_ip = '10.149.21.15'  # VMware_API.turn_on_vm_and_get_ip('18')
    # node_API.run_initial_setup(idle_node_9_ip)
    # node_API.run_pip_installers(idle_node_9_ip)
    # node_API.run_rabbitmq_setup(idle_node_9_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_9_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_9_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_10_ip = '10.149.21.16'  # VMware_API.turn_on_vm_and_get_ip('19')
    # node_API.run_initial_setup(idle_node_10_ip)
    # node_API.run_pip_installers(idle_node_10_ip)
    # node_API.run_rabbitmq_setup(idle_node_10_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_10_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_10_ip,))
    processThread.start()
    threads.append(processThread)

    for thread in threads:
        print(f'Joining thread {thread}')
        thread.join()

    add_node(node.Node('10', 'IDLE', idle_node_1_ip, '51820'))
    add_node(node.Node('11', 'IDLE', idle_node_2_ip, '51820'))
    add_node(node.Node('12', 'IDLE', idle_node_3_ip, '51820'))
    add_node(node.Node('13', 'IDLE', idle_node_4_ip, '51820'))
    add_node(node.Node('14', 'IDLE', idle_node_5_ip, '51820'))
    add_node(node.Node('15', 'IDLE', idle_node_6_ip, '51820'))
    add_node(node.Node('16', 'IDLE', idle_node_7_ip, '51820'))
    add_node(node.Node('17', 'IDLE', idle_node_8_ip, '51820'))
    add_node(node.Node('18', 'IDLE', idle_node_9_ip, '51820'))
    add_node(node.Node('19', 'IDLE', idle_node_10_ip, '51820'))

    node_API.set_id(idle_node_1_ip, '10')
    node_API.set_id(idle_node_2_ip, '11')
    node_API.set_id(idle_node_3_ip, '12')
    node_API.set_id(idle_node_4_ip, '13')
    node_API.set_id(idle_node_5_ip, '14')
    node_API.set_id(idle_node_6_ip, '15')
    node_API.set_id(idle_node_7_ip, '16')
    node_API.set_id(idle_node_8_ip, '17')
    node_API.set_id(idle_node_9_ip, '18')
    node_API.set_id(idle_node_10_ip, '19')


def boot_and_init_setup_pipeline_nodes():
    threads = []
    print(
        f'------------------------------------------------------------------------------------------------------------> Starting pipeline nodes')
    split_node_ip = '10.149.21.1'  # VMware_API.turn_on_vm_and_get_ip('1')
    processThread = threading.Thread(target=init_idle_node, args=(split_node_ip,))
    processThread.start()
    threads.append(processThread)

    blur_node_ip = '10.149.21.2'  # VMware_API.turn_on_vm_and_get_ip('2')
    processThread = threading.Thread(target=init_idle_node, args=(blur_node_ip,))
    processThread.start()
    threads.append(processThread)

    stabilize_video_node_ip = '10.149.21.3'  # VMware_API.turn_on_vm_and_get_ip('3')
    processThread = threading.Thread(target=init_idle_node, args=(stabilize_video_node_ip,))
    processThread.start()
    threads.append(processThread)

    add_watermark_node_ip = '10.149.21.5'  # VMware_API.turn_on_vm_and_get_ip('4')
    processThread = threading.Thread(target=init_idle_node, args=(add_watermark_node_ip,))
    processThread.start()
    threads.append(processThread)

    reverse_node_ip = '10.149.21.18'  # VMware_API.turn_on_vm_and_get_ip('5')
    processThread = threading.Thread(target=init_idle_node, args=(reverse_node_ip,))
    processThread.start()
    threads.append(processThread)

    vintage_node_ip = '10.149.21.19'  # VMware_API.turn_on_vm_and_get_ip('6')
    processThread = threading.Thread(target=init_idle_node, args=(vintage_node_ip,))
    processThread.start()
    threads.append(processThread)

    merge_parts_node_ip = '10.149.21.4'  # VMware_API.turn_on_vm_and_get_ip('7')
    processThread = threading.Thread(target=init_idle_node, args=(merge_parts_node_ip,))
    processThread.start()
    threads.append(processThread)

    for thread in threads:
        print(f'Joining {thread}')
        thread.join()

    add_node(node.Node('1', filter_types.SPLIT, split_node_ip, '51821'))
    add_node(node.Node('2', filter_types.BLUR, blur_node_ip, '51822'))
    add_node(node.Node('3', filter_types.BLUR, stabilize_video_node_ip, '51823'))
    add_node(node.Node('4', filter_types.BLUR, add_watermark_node_ip, '51824'))
    add_node(node.Node('5', filter_types.BLUR, reverse_node_ip, '51825'))
    add_node(node.Node('6', filter_types.BLUR, vintage_node_ip, '51826'))
    add_node(node.Node('7', filter_types.MERGE_PARTS, merge_parts_node_ip, '51827'))

    node_API.set_id(split_node_ip, '1')
    node_API.set_id(blur_node_ip, '2')
    node_API.set_id(stabilize_video_node_ip, '3')
    node_API.set_id(add_watermark_node_ip, '4')
    node_API.set_id(reverse_node_ip, '5')
    node_API.set_id(vintage_node_ip, '6')
    node_API.set_id(merge_parts_node_ip, '7')

    return split_node_ip, blur_node_ip, stabilize_video_node_ip, add_watermark_node_ip, reverse_node_ip, vintage_node_ip, merge_parts_node_ip


def save_connection(id_1, id_2, interface_name_1, interface_name_2, subnet):
    # print(f'SAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE {}')
    node_1 = get_node(id_1)
    node_1.connections.append(
        connection.Connection(id_2, subnet, interface_name_1, is_consumer=False))  # connection with node 2

    node_2 = get_node(id_2)
    node_2.connections.append(connection.Connection(id_1, subnet, interface_name_2, is_consumer=True))

    # print(
    # f' (iiiiiiiiiiiiiiiiiiiiiiiiii) Saving connection {id_1} -> {id_2}, interface_1 {interface_name_1}, interface_2: {interface_name_2},  subnet: {subnet}')


def start_pipeline():
    split_node_ip, blur_node_ip_1, blur_node_ip_2, blur_node_ip_3, blur_node_ip_4, blur_node_ip_5, blur_node_ip_6 = boot_and_init_setup_pipeline_nodes()
    boot_and_init_idle_nodes()
    ################################################################################################
    node_API.launch_checker_daemon(split_node_ip)
    # node_API.launch_ffmpeg_last_run_check(split_node_ip)
    node_API.setup_consumer('None', split_node_ip, filter_types.SPLIT)
    node_API.setup_producer(split_node_ip, SPLIT_NODE_LO)
    #node_API.start_socket(split_node_ip, controller_ip)

    ############################
    interface_1, interface_2 = node_API.connect_nodes(split_node_ip, blur_node_ip_1, '1', '2', str(get_port()),
                                                      '24adna',
                                                      '10.0.1.')
    save_connection('1', '2', interface_1, interface_2, '10.0.1.x')

    node_API.launch_checker_daemon(blur_node_ip_1)
    #node_API.launch_ffmpeg_last_run_check(blur_node_ip_1)
    node_API.setup_consumer(SPLIT_NODE_LO, blur_node_ip_1,
                            filter_types.BLUR)  # split_node_ip should be replaced with loopback
    node_API.setup_producer(blur_node_ip_1, BLUR_NODE_LO)
    #node_API.start_socket(blur_node_ip_1, controller_ip)
    # NEW CODE
    # node_API.setup_producer(vm_ip=blur_node_ip_1, loopback_addr=BLUR_NODE_LO, queue_name='first',
    #                         folder_name='distributed_files')
    node_API.setup_consumer(BLUR_NODE_LO, blur_node_ip_1,
                            filter_types.BLUR, third_argument='first')  # split_node_ip should be replaced with loopback
    node_API.launch_receive_files_process(vm_ip=blur_node_ip_1, host_ip=BLUR_NODE_LO)
    ################################################################################################ CONNECT BLUR NODE TO STABILIZE
    interface_1, interface_2 = node_API.connect_nodes(split_node_ip, blur_node_ip_2, '1', '3', str(get_port()),
                                                      '24adna',
                                                      '10.0.2.')
    save_connection('1', '3', interface_1, interface_2, '10.0.2.x')

    node_API.launch_checker_daemon(blur_node_ip_2)
    #node_API.launch_ffmpeg_last_run_check(blur_node_ip_2)
    node_API.setup_consumer(SPLIT_NODE_LO, blur_node_ip_2, filter_types.BLUR)
    node_API.setup_producer(blur_node_ip_2, STABILIZE_NODE_LO)
    #node_API.start_socket(blur_node_ip_2, controller_ip)
    # NEW CODE
    # node_API.setup_producer(vm_ip=blur_node_ip_2, loopback_addr=STABILIZE_NODE_LO, queue_name='first',
    #                         folder_name='distributed_files')
    node_API.setup_consumer(STABILIZE_NODE_LO, blur_node_ip_2,
                            filter_types.BLUR, third_argument='first')  # split_node_ip should be replaced with loopback
    node_API.launch_receive_files_process(vm_ip=blur_node_ip_2, host_ip=STABILIZE_NODE_LO)
    ################################################################################################ CONNECT STABILIZE TO ADD_WATERMARK
    interface_1, interface_2 = node_API.connect_nodes(split_node_ip, blur_node_ip_3, '1', '4', str(get_port()),
                                                      '24adna',
                                                      '10.0.3.')
    save_connection('1', '4', interface_1, interface_2, '10.0.3.x')

    node_API.launch_checker_daemon(blur_node_ip_3)
    #node_API.launch_ffmpeg_last_run_check(blur_node_ip_3)
    node_API.setup_consumer(SPLIT_NODE_LO, blur_node_ip_3, filter_types.BLUR)
    node_API.setup_producer(blur_node_ip_3, ADD_WATERMARK_NODE_LO)
    #node_API.start_socket(blur_node_ip_3, controller_ip)
    # NEW CODE
    # node_API.setup_producer(vm_ip=blur_node_ip_3, loopback_addr=ADD_WATERMARK_NODE_LO, queue_name='first',
    #                         folder_name='distributed_files')
    node_API.setup_consumer(ADD_WATERMARK_NODE_LO, blur_node_ip_3,
                            filter_types.BLUR, third_argument='first')  # split_node_ip should be replaced with loopback
    node_API.launch_receive_files_process(vm_ip=blur_node_ip_3, host_ip=ADD_WATERMARK_NODE_LO)
    ################################################################################################ CONNECT ADD_WATERMARK TO REVERSE
    interface_1, interface_2 = node_API.connect_nodes(split_node_ip, blur_node_ip_4, '1', '5', str(get_port()),
                                                      '24adna',
                                                      '10.0.4.')
    save_connection('1', '5', interface_1, interface_2, '10.0.4.x')

    node_API.launch_checker_daemon(blur_node_ip_4)
    #node_API.launch_ffmpeg_last_run_check(blur_node_ip_4)
    node_API.setup_consumer(SPLIT_NODE_LO, blur_node_ip_4, filter_types.BLUR)
    node_API.setup_producer(blur_node_ip_4, REVERSE_NODE_LO)
    #node_API.start_socket(blur_node_ip_4, controller_ip)
    # NEW CODE
    # node_API.setup_producer(vm_ip=blur_node_ip_4, loopback_addr=REVERSE_NODE_LO, queue_name='first',
    #                         folder_name='distributed_files')
    node_API.setup_consumer(REVERSE_NODE_LO, blur_node_ip_4,
                            filter_types.BLUR, third_argument='first')  # split_node_ip should be replaced with loopback
    node_API.launch_receive_files_process(vm_ip=blur_node_ip_4, host_ip=REVERSE_NODE_LO)
    ################################################################################################ CONNECT ADD_WATERMARK TO REVERSE
    interface_1, interface_2 = node_API.connect_nodes(split_node_ip, blur_node_ip_5, '1', '6', str(get_port()),
                                                      '24adna',
                                                      '10.0.5.')
    save_connection('1', '6', interface_1, interface_2, '10.0.5.x')

    node_API.launch_checker_daemon(blur_node_ip_5)
    #node_API.launch_ffmpeg_last_run_check(blur_node_ip_5)
    node_API.setup_consumer(SPLIT_NODE_LO, blur_node_ip_5, filter_types.BLUR)
    node_API.setup_producer(blur_node_ip_5, VINTAGE_NODE_LO)
   # node_API.start_socket(blur_node_ip_5, controller_ip)
    # NEW CODE
    # node_API.setup_producer(vm_ip=blur_node_ip_5, loopback_addr=VINTAGE_NODE_LO, queue_name='first',
    #                         folder_name='distributed_files')
    node_API.setup_consumer(VINTAGE_NODE_LO, blur_node_ip_5,
                            filter_types.BLUR, third_argument='first')  # split_node_ip should be replaced with loopback
    node_API.launch_receive_files_process(vm_ip=blur_node_ip_5, host_ip=VINTAGE_NODE_LO)
    ################################################################################################ CONNECT ADD_WATERMARK TO MERGE_PARTS
    # interface_1, interface_2 = node_API.connect_nodes(split_node_ip, blur_node_ip_6, '1', '7', str(get_port()),
    #                                                   '24adna',
    #                                                   '10.0.6.')
    # save_connection('1', '7', interface_1, interface_2, '10.0.6.x')
    #
    # node_API.launch_checker_daemon(blur_node_ip_6)
    # node_API.setup_consumer(SPLIT_NODE_LO, blur_node_ip_6, filter_types.BLUR)
    # node_API.start_socket(blur_node_ip_6, controller_ip)
    # node_API.run_merge_parts_service(blur_node_ip_6)


def compute_remaining_processing_time(q_size, node_filter):
    pass


def get_least_used_node(bottleneck_id):
    # print(
    # f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ GET LEAST USED NODE TO SCALE {bottleneck_id}')

    for node in nodes:

        if node.id != '1' and node.id != '2' and node.id != '3' and node.id != '6' and node.id != '4' and node.id != '5' and node.id != '7':
            # check the queue size and see if its empty
            # print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ GET CUMULATIVE QUEUE SIZE OF {node.id}')
            q_size = node_API.get_cumulative_queue_size(node.ip)
            if q_size == 0 and not node_already_connected_to(node.id, bottleneck_id) and not node_API.check_ffmpeg_runs(
                    node.ip):
                print(f'Least used node is {node.id}')
                return node
    return None


def node_already_connected_to(this_node_id, connected_to_id):
    this_node = get_node(this_node_id)
    this_node_connections = this_node.connections
    for connection in this_node_connections:
        if connection.with_node_id == connected_to_id:
            return True
    return False


def setup_connections(node1_ip, node1_id, node1_lo, node1_next_port, node1_function, next_subnet, node2_ip, node2_id,
                      node2_lo, controller_ip):
    bottleneck_interface, idle_interface = node_API.connect_nodes(node1_ip, node2_ip,
                                                                  node1_id, node2_id,
                                                                  node1_next_port, '24adna',
                                                                  next_subnet.replace('x', ''))

    set_last_used_port(node1_id, node1_next_port)
    save_connection(node1_id, node2_id, bottleneck_interface, idle_interface, next_subnet)

    idle_node_function = get_subsequent_function(node1_function)

    get_node(node2_id).function = idle_node_function

    # node_creator.setup_consumer(node1_lo, node2_ip, idle_node_function)
    node_API.launch_checker_daemon(node2_ip)
    #node_API.launch_ffmpeg_last_run_check(node2_ip)

    node_API.setup_consumer(queue_ip=node1_lo, vm_ip=node2_ip,
                            task_type=idle_node_function)  # idle node consumes from bottleneck

    node_API.setup_producer(vm_ip=node2_ip, loopback_addr=node2_lo)
    #node_API.start_socket(node2_ip, controller_ip)

    subs_nodes = get_subsequent_nodes_of(idle_node_function, node2_id, idle_node_function)
    time.sleep(3)
    for node in subs_nodes:
        # no actual wireguard setup for sub nodes?
        # print(
        # f'==================================================================================================== Consumer of idle node {node2_id} will be {node.id} with ip {node.ip} and func {node.function}')
        # setup wireguard for consumer node
        # print('SETUP WIREGUARD FOR CONSUMER NODE')

        node_2 = get_node(node2_id)
        next_port_idle = str(int(node_2.las_used_port) + 1)
        node_subnets = get_subnets_of_node(node.id)
        node_subnets.append(get_subnets_of_node(node2_id))  # make sure no subnet conflicts between the 2 nodes
        next_node_subnet = get_next_subnet(node_subnets)

        interface1, interfac2 = node_API.connect_nodes(producer_ip=node2_ip, consumer_ip=node.ip, producer_id=node2_id,
                                                       consumer_id=node.id, port_producer=next_port_idle,
                                                       vm_passwd='24adna', subnet=next_node_subnet.replace('x', ''),
                                                       lo_flag=False)
        save_connection(id_1=node2_id, id_2=node.id, interface_name_1=interface1, interface_name_2=interfac2,
                        subnet=next_node_subnet)

        node_API.launch_checker_daemon(node.ip)
        node_API.setup_consumer(queue_ip=node2_lo, vm_ip=node.ip, task_type=node.function)


def assign_node(idle_node, bottleneck_id, bottleneck_ip, bottleneck_function, bottleneck_lo):
    idle_node_id = idle_node.id
    idle_node_ip = idle_node.ip
    idle_node_lo = '172.16.0.' + idle_node_id
    next_port_producer = str(int(get_node_last_used_port(bottleneck_id)) + 1)

    # print(f'@@@@@@@@@@@@@@@@@ Idle node {idle_node_id} connecting to bottleneck node: {bottleneck_id}')

    current_subnets = get_subnets_of_node(bottleneck_id)
    next_subnet = get_next_subnet(current_subnets)
    # print(f'++++++++++++++ Next subnet of node node {idle_node_id} will be {next_subnet}')
    setup_connections(bottleneck_ip, bottleneck_id, bottleneck_lo, next_port_producer, bottleneck_function, next_subnet,
                      idle_node_ip, idle_node_id, idle_node_lo, controller_ip)


def get_number_of_consumers(of_node):
    nr_consumers = 0
    for connection in of_node.connections:
        if not connection.is_consumer:  # i am producer
            nr_consumers += 1
    return nr_consumers


def compute_remaining_work(of_bottleneck_node, nr_elements):
    # TO DO: Fill correct average for each filter
    # Only works for BLUR, STABILIZE
    remaining_work = -1
    nr_of_consumers = get_number_of_consumers(of_bottleneck_node)
    if nr_of_consumers != 0:
        if of_bottleneck_node.function == filter_types.SPLIT:
            remaining_work = (12 * nr_elements) / nr_of_consumers
        elif of_bottleneck_node.function == filter_types.BLUR:
            remaining_work = (12 * nr_elements) / nr_of_consumers
        elif of_bottleneck_node.function == filter_types.STABILIZE:
            remaining_work = (12 * nr_elements) / nr_of_consumers
        elif of_bottleneck_node.function == filter_types.ADD_WATERMARK:
            remaining_work = (12 * nr_elements) / nr_of_consumers
        elif of_bottleneck_node.function == filter_types.REVERSE:
            remaining_work = (12 * nr_elements) / nr_of_consumers
        elif of_bottleneck_node.function == filter_types.VINTAGE:
            remaining_work = (12 * nr_elements) / nr_of_consumers

    # print(
    # f'================================================================================================== REMAINING WORK: {remaining_work} seconds with {nr_of_consumers} consumers')
    return remaining_work


def reset_consumer_of_node(node_obj, least_used_node):
    node_API.kill_process(vm_ip=node_obj.ip, process_name='checker_daemon.py')
    node_API.delete_checker_daemon_config_line(vm_ip=node_obj.ip, line=f'172.16.0.{least_used_node.id}',
                                               file_to_delete='consumer.txt')
    node_API.kill_process(vm_ip=node_obj.ip, process_name=f'172.16.0.{least_used_node.id}')
    node_API.launch_checker_daemon(vm_ip=node_obj.ip)


def consume_from_queue_with_function(node_obj, q_name):
    if not q_name:
        return get_subsequent_function(node_obj.function)

    crt_q_function = node_obj.function
    base_name = 'hello'

    counter = 2

    # If q is hello, then you should conume with function get_subseq(bottleneck_function)
    if q_name == base_name:
        return get_subsequent_function(crt_q_function)

    while True:
        current_q_name = base_name + str(counter)
        crt_q_function = get_subsequent_function(crt_q_function)

        if current_q_name == q_name:
            return get_subsequent_function(crt_q_function)

        if counter > 7:
            print(
                ')))))))))))))))))))))))))))))))))))))))))))))))))))))))))) WRRRRRRRRRRRRRRRRRRRRRRROOOOOOOOOOOOOOOOOOOOOOOOOONGGGGGGGGGGGG')
            return

        counter += 1


def scale_up_node_with_least_used_node(bottleneck_node, least_used_node, queue_name):
    """

    :param bottleneck_node:
    :param least_used_node:
        State description:
            First time state:
                Connected to node 1 as a consumer
                Connected to node 3 as a producer with default queue
                Connected to node 4 as a producer with queue hello2
                Connected to node 5 as a producer with queue hello3
                Connected to node 6 as a producer with queue hello4
                Connected to node 7 as a producer with queue hello5
            Upon scale-up:
                Resetting:
                    Stop all self consuming activities including producer, consumers, delete queues
                    Stop consuming from node 1
                    Delete all self consuming producer folders
                    Disconnect from all nodes by running the kill 172.16.0.least_id command
                    Remove all checker daemon lines where there is 172.16.0.least_id from all nodes
                    Remove consumer.txt file from least_used node
                    Remove producer.txt file from least_used node
                Scale-up:
                    Least used node will consume from bottleneck
                    Subsequent nodes will consume from least_used


    :return:

    """

    old_function_least = least_used_node.function
    least_used_node_function = consume_from_queue_with_function(bottleneck_node, q_name=queue_name)

    node_API.track_reassign(vm_ip=least_used_node.ip, old_function=old_function_least,
                            new_function=least_used_node_function, from_q=bottleneck_node.id, start_or_stop='START')

    # First time state resetting least used node
    print(
        f'----------------------- Resetting least used node {least_used_node.id} {least_used_node.ip} ---------------')
    node_API.kill_process(vm_ip=least_used_node.ip, process_name='checker_daemon.py')
    node_API.delete_checker_daemon_config_line(vm_ip=least_used_node.ip, file_to_delete='consumer.txt')
    node_API.delete_checker_daemon_config_line(vm_ip=least_used_node.ip, file_to_delete='producer.txt')
    node_API.kill_process(vm_ip=least_used_node.ip, process_name='consumer.py')
    node_API.kill_process(vm_ip=least_used_node.ip, process_name='producer.py')
    node_API.delete_all_self_consume_producer_folders(vm_ip=least_used_node.ip)
    node_API.reset_rabbitmq(vm_ip=least_used_node.ip)
    node_API.run_rabbitmq_setup(vm_ip=least_used_node.ip)
    node_API.delete_leftover_mp4(vm_ip=least_used_node.ip)

    # First time state reset connection of least_used_node with all other nodes
    threads = []
    print(f'-------------------------- Resetting consumers of least used node {least_used_node.id} -------------------')
    for node_obj in nodes:
        if node_obj.id != least_used_node.id and node_obj.id in pipeline_nodes:
            print(f'--------------- Resetting consumer for node {node_obj.id} ----------------')
            processThread = threading.Thread(target=reset_consumer_of_node, args=(node_obj, least_used_node,))
            processThread.start()
            threads.append(processThread)

    for thread in threads:
        print(f'Joining thread {thread}')
        thread.join()

    # Connect least_used_node to bottleneck
    print(
        f'---------------------------------->> Connect least_used_node {least_used_node.id} to bottleneck_node {bottleneck_node.id} --------')

    print(
        f'((((((((((((((((((((((((((((((( LEAST USED NODE FUNCTION FOR Q_NAME {queue_name} is {least_used_node_function}')
    # least_used_node_function = get_subsequent_function(bottleneck_node.function)

    node_API.track_reassign(vm_ip=least_used_node.ip, old_function=old_function_least,
                            new_function=least_used_node_function, from_q=bottleneck_node.id, start_or_stop='END')

    node_API.setup_consumer(queue_ip=f'172.16.0.{bottleneck_node.id}', vm_ip=least_used_node.ip,
                            task_type=least_used_node_function, queue_name=queue_name)
    node_API.setup_producer(vm_ip=least_used_node.ip, loopback_addr=f'172.16.0.{least_used_node.id}')
    node_API.launch_checker_daemon(vm_ip=least_used_node.ip)
    # Subsequent nodes consume from least_used_node hello queue
    subsequent_nodes = get_subsequent_nodes_of(node_function=least_used_node_function, id=least_used_node.id,
                                               func=least_used_node_function)
    for subsequent_node in subsequent_nodes:
        print(
            f'-------------->>> Subsequent node {subsequent_node.id} consuming from least_used_node {least_used_node.id} ----------')
        node_API.setup_consumer(queue_ip=f'172.16.0.{least_used_node.id}', vm_ip=subsequent_node.ip,
                                task_type=subsequent_node.function)

    # Reset least_used_node object
    print(f'--------------- Resetting least used node {least_used_node.id} object ')
    # least_used_node = get_node(least_used_node.id)
    least_used_node = node.Node(id=least_used_node.id, func=least_used_node_function, ip=least_used_node.ip,
                                port=least_used_node.las_used_port)
    print(f'----- LEast used node {least_used_node.id} function before self_scale_worker {least_used_node.function}')
    self_scale_worker(least_used_node, require_mutex=False)
    # time.sleep(4)


def get_next_producer_folder_name(node_obj):
    current_queue_index = node_obj.current_queue_index
    next_queue_index = current_queue_index + 1
    return filter_types.SECOND_PRODUCER_FOLDER_NAME + str(next_queue_index)


def get_current_queue_name(node_obj):
    queue_name = filter_types.DEFAULT_QUEUE_NAME
    current_queue_index = node_obj.current_queue_index
    if current_queue_index == 1:
        return queue_name
    return queue_name + str(current_queue_index)


def get_next_self_queue(node_obj):
    current_queue_index = node_obj.current_queue_index
    next_queue_index = current_queue_index + 1
    queue_name = 'hello' + str(next_queue_index)
    return queue_name


def get_last_queue_size(node_obj):
    q_name = get_current_queue_name(node_obj)
    q_size = node_API.get_q_size(node_obj.ip, q_name)
    # print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ LAST_QUEUE NAME {q_name} SIZE {q_size} ')
    return q_size


def get_function_level(func):
    if func == filter_types.SPLIT:
        return 1
    if func == filter_types.BLUR:
        return 2
    if func == filter_types.STABILIZE:
        return 3
    if func == filter_types.ADD_WATERMARK:
        return 4
    if func == filter_types.REVERSE:
        return 5
    if func == filter_types.VINTAGE:
        return 6
    if func == filter_types.MERGE_PARTS:
        return 7
    return -1


def all_above_queues_have_been_consumed(node_obj):
    # print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ ALL ABOVE Q SUM of {node_obj.id}')
    node_obj_level = get_function_level(node_obj.function)
    sum = 0
    for node in nodes:
        if node.id in pipeline_nodes:
            crt_node_level = get_function_level(node.function)
            if crt_node_level < node_obj_level:
                q_size = node_API.get_q_size(node.ip)
                # print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ ADD Q SIZE {q_size} OF NODE {node.id}')
                sum += q_size
    # print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ ALL ABOVE Q SUM of {node_obj.id} SUM {sum}')
    if sum == 0:
        return True
    return False


def self_scale(bottleneck_lo, bottleneck_ip, bottleneck_id, bottleneck_node_obj):
    # consume its own queue to prevent idle CPU
    # ISSUE: hello2 sub_function is not the same as hello3 sub_function
    # ISSUE: which queue size matters for self sclaing?
    bottleneck_function = bottleneck_node_obj.last_function
    if bottleneck_node_obj.last_function == filter_types.IDLE:
        bottleneck_function = bottleneck_node_obj.function

    # print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ last_function {bottleneck_function}')
    # node_API.setup_consumer()
    current_queue_name = get_current_queue_name(bottleneck_node_obj)  # hello
    # print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[  current queue {current_queue_name}')
    next_queue_name = get_next_self_queue(bottleneck_node_obj)  # hello2
    # print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ next queue {next_queue_name} ')
    subsequent_function = get_subsequent_function(bottleneck_function)
    # print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ subsequent func {subsequent_function}')
    next_producer_folder_name = get_next_producer_folder_name(bottleneck_node_obj)
    # print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ next producer folder name {next_producer_folder_name}')
    # self consume and put output in second producer folder
    print(
        f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ node {bottleneck_id} SHOULD SELF CONSUME from {bottleneck_function} to {subsequent_function}')
    node_API.create_next_producer_folder(bottleneck_ip, next_producer_folder_name)
    node_API.setup_consumer(queue_ip=bottleneck_lo, vm_ip=bottleneck_ip, task_type=subsequent_function,
                            third_argument=next_producer_folder_name, forth_argument=current_queue_name,
                            node_id=bottleneck_id)  # save output from self consume to new folder
    # setup second queue
    node_API.setup_producer(bottleneck_ip, bottleneck_lo, next_queue_name, next_producer_folder_name)
    # setup consumer to consume from second queue

    subsequenct_nodes = get_subsequent_nodes_of(subsequent_function, bottleneck_id, subsequent_function)
    print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[  SUBSEQUENT NODES')
    print(subsequenct_nodes)

    for subsequent_node in subsequenct_nodes:
        if subsequent_node.id == LAST_PIPELINE_NODE:
            print(
                f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ NODE {subsequent_node.id} CONSUMING FROM  QUEUE {next_queue_name}')
            port = str(get_port())
            subnet = get_subnet()
            interface_1, interface_2 = node_API.connect_nodes(producer_ip=bottleneck_ip, consumer_ip=subsequent_node.ip,
                                                              producer_id=bottleneck_id, consumer_id=subsequent_node.id,
                                                              port_producer=port,
                                                              vm_passwd='24adna',
                                                              subnet=subnet)
            save_connection(bottleneck_id, subsequent_node.id, interface_1, interface_2, f'{subnet}.x')

            node_API.launch_checker_daemon(subsequent_node.ip)
            node_API.setup_consumer(bottleneck_lo, subsequent_node.ip, subsequent_node.function,
                                    next_queue_name, node_id=subsequent_node.id)
            #node_API.start_socket(subsequent_node.ip, controller_ip)
            node_API.run_merge_parts_service(subsequent_node.ip)

            subsequent_node.las_used_port = port
            subsequent_node.function = filter_types.MERGE_PARTS

            # Save connection to object
            for subsequent_node_connection in subsequent_node.connections:
                if subsequent_node_connection.with_node_id == bottleneck_id:
                    subsequent_node_connection.q_names.append(next_queue_name)
                    break
    # All nodes are consumers of node 1

    # consumer_node_function = (get_subsequent_function(subsequent_function))
    # place_in_folder = get_producer_folder2(consumer_node_function)
    # consume_from_queue = next_queue_name
    #
    # if consumer_node_function != filter_types.MERGE_PARTS:
    #     for consumer_node in nodes:
    #         if consumer_node.id != bottleneck_id and consumer_node.id != LAST_PIPELINE_NODE and consumer_node.id != '1' and consumer_node_function != filter_types.MERGE_PARTS:
    #             print(f"??????????????????????????????????? SUBSEQUENT NODE {consumer_node.id} WILL CONSUME FROM {bottleneck_id} {next_queue_name}"
    #                   f" AND PLACE OUTPUT IN {place_in_folder} USING FUNCTION {consumer_node_function}")
    #             node_API.setup_consumer(queue_ip=bottleneck_lo, vm_ip=consumer_node.ip, task_type=consumer_node_function,
    #                                     third_argument=place_in_folder, forth_argument=consume_from_queue, node_id=consumer_node.id)

    bottleneck_node_obj.current_queue_index += 1
    bottleneck_node_obj.last_function = subsequent_function


def get_producer_folder2(function):
    # if node 2,3,4...10,11..19
    if function == filter_types.STABILIZE:
        return filter_types.SECOND_PRODUCER_FOLDER_NAME + '2'
    if function == filter_types.ADD_WATERMARK:
        return filter_types.SECOND_PRODUCER_FOLDER_NAME + '3'
    if function == filter_types.REVERSE:
        return filter_types.SECOND_PRODUCER_FOLDER_NAME + '4'
    if function == filter_types.VINTAGE:
        return filter_types.SECOND_PRODUCER_FOLDER_NAME + '5'

def get_all_q_names(q_list):
    msg = ' '
    for q_name in q_list:
        msg += f'{q_name} AND '
    return msg


def print_node_connected_to(node_obj):
    msg = f'(INFOOOO) Node_id {node_obj.id}  '
    for node_connection in node_obj.connections:
        msg += f'CONSUMES FROM {node_connection.with_node_id} : {node_connection.is_consumer}  '
        if node_connection.is_consumer:
            msg += ' QUEUE NAMES ' + get_all_q_names(node_connection.q_names)
        else:
            msg += ' | '
    print(msg)


def assign_all_idle_nodes_to_1():
    # print('ASSIGNING ALL IDLE NODES TO FIRST PIPELINE NODE')
    # assign all idle nodes to first pipeline node
    # crt_idle_node = get_idle_node()
    split_node = get_node('1')
    crt_idle_node = get_idle_node()
    crt_port = 51820

    while crt_idle_node:
        print(f'ASSSSSSSSSIGN IDLE NODE {crt_idle_node.id}')
        ################################################################################################ CONNECT ADD_WATERMARK TO MERGE_PARTS
        port = str(get_port())
        interface_1, interface_2 = node_API.connect_nodes(split_node.ip, crt_idle_node.ip, '1', crt_idle_node.id, port,
                                                          '24adna',
                                                          f'10.0.{crt_idle_node.id}.')
        save_connection('1', crt_idle_node.id, interface_1, interface_2, f'10.0.{crt_idle_node.id}.x')

        node_API.launch_checker_daemon(crt_idle_node.ip)
        #node_API.launch_ffmpeg_last_run_check(crt_idle_node.ip)
        node_API.setup_consumer(SPLIT_NODE_LO, crt_idle_node.ip, filter_types.BLUR)
        node_API.setup_producer(vm_ip=crt_idle_node.ip, loopback_addr=f'172.16.0.{crt_idle_node.id}')
        ## NEW SETUP
        # node_API.setup_producer(vm_ip=crt_idle_node.ip, loopback_addr=f'172.16.0.{crt_idle_node.id}', queue_name='first',
        #                         folder_name='distributed_files')
        node_API.setup_consumer(f'172.16.0.{crt_idle_node.id}', crt_idle_node.ip,
                                filter_types.BLUR,
                                third_argument='first')  # split_node_ip should be replaced with loopback
        node_API.launch_receive_files_process(vm_ip=crt_idle_node.ip, host_ip=f'172.16.0.{crt_idle_node.id}')
        # for node in nodes:
        #     if node.id != crt_idle_node.id:
        #         node_API.setup_consumer(queue_ip=f'172.16.0.{crt_idle_node.id}', vm_ip=node.ip, task_type=filter_types.STABILIZE,
        #                                 third_argument=filter_types.SECOND_PRODUCER_FOLDER_NAME+'2', forth_argument='hello')
        #Setup consumers of this producer which is all other nodes
        #node_API.start_socket(crt_idle_node.ip, controller_ip)
        # node_API.run_merge_parts_service(crt_idle_node.ip)

        crt_idle_node.las_used_port = port
        crt_idle_node.function = filter_types.BLUR

        crt_idle_node = get_idle_node()


def least_used_scaler(node):
    global available_scale_up_nodes
    bottleneck_id = node.id
    ssh_instance = node_API.get_ssh_instance(vm_ip_address=node.ip, vm_passwd='24adna')
    while True:
        bottleneck_node_obj = get_node(bottleneck_id)
        bottleneck_ip = bottleneck_node_obj.ip
        bottleneck_q_size = node_API.get_q_size(bottleneck_ip, ssh_instance=ssh_instance)
        bottleneck_function = get_node_function(bottleneck_id)
        bottleneck_lo = '172.16.0.' + bottleneck_id
        get_node(bottleneck_id).nr_elements_q = bottleneck_q_size
        remaining_work = compute_remaining_work(bottleneck_node_obj, bottleneck_q_size)

        scaled_up = False

        # if bottleneck_q_size > 0:
        # print(f'{bottleneck_id} has positive default q size {bottleneck_q_size} ')

        most_used_ss_q_name = None
        most_used_ss_q_size = 0

        if bottleneck_id not in pipeline_nodes:
            cumulative_q, most_used_ss_q_name, most_used_ss_q_size = node_API.get_cumulative_queue_size(node.ip,
                                                                                                        ssh_instance=ssh_instance)
            ffmpeg_has_not_ran = node_API.ffmpeg_has_not_ran_in_x_seconds(vm_ip=bottleneck_ip, seconds=10,
                                                                          ssh_instance=ssh_instance)

            if cumulative_q == 0 and ffmpeg_has_not_ran:
                if bottleneck_id not in available_scale_up_nodes and mutex_self_scale.acquire(False):
                    print(f'! ! ! ! ! ! ! ! ! ! Available node for scale_up found! {bottleneck_id}')
                    available_scale_up_nodes.append(bottleneck_id)
                    print(available_scale_up_nodes)
                    mutex_self_scale.release()
            elif not ffmpeg_has_not_ran or cumulative_q > 0:
                if mutex_self_scale.acquire(True):
                    # print(f'Try to remove; Available scaleup nodes for {bottleneck_id} {available_scale_up_nodes}')
                    try:
                        available_scale_up_nodes.remove(bottleneck_id)
                        print(
                            f' :( :( :( :( :( Node no longer available: {bottleneck_id} because ffmpeg_has_not_ran {ffmpeg_has_not_ran} or cumulative_q {cumulative_q} > 0')
                    except:
                        pass
                    mutex_self_scale.release()

        # bottleneck_last_queue_size = get_last_queue_size(bottleneck_node_obj)

        if bottleneck_id != '1' and bottleneck_id != PENULTIM_PIPELINE_NODE and bottleneck_function != PENULTIM_PIPELINE_FUNCTION and (
                bottleneck_q_size > 1 or (
                most_used_ss_q_name and most_used_ss_q_size > 1)):  # because then helper node will replicate node 6
            if mutex_self_scale.acquire(False):
                # print(f'Node {bottleneck_id} GOT the mutex 1')
                # print(f'Node {bottleneck_id} Get idle nodes')
                idle_node = get_idle_node()
                if idle_node is None:
                    least_used_node = None
                    try:
                        # print(f'Node {bottleneck_id} Get least used node from {available_scale_up_nodes}')
                        # least_used_node = get_least_used_node(bottleneck_id) # node must have q size 0
                        least_used_node = None
                        try:
                            least_used_node_id = available_scale_up_nodes[0]
                            available_scale_up_nodes.remove(least_used_node_id)
                            least_used_node = get_node(least_used_node_id)
                        except:
                            pass
                            # print('No available node to scale up')
                    except ValueError:
                        print('ValueError EXCEPTION at getting least used node')
                    if least_used_node:
                        print(f'{bottleneck_id} Got least used node, check ffmpeg runs')
                        ffmpeg_running = node_API.check_ffmpeg_runs(least_used_node.ip) or node_API.downloaded_mp4(
                            vm_ip=least_used_node.ip)
                        check_least_node_cum = node_API.get_cumulative_queue_size_CLI(least_used_node.ip)
                        if ffmpeg_running or check_least_node_cum > 0:
                            print(
                                f'{bottleneck_id} Ffmpeg runing / downloaded file present {ffmpeg_running}/ cum > 0 {check_least_node_cum > 0}, IGNORE ****************************$$$$$*******************************')
                            # node_API.gracefully_stop_consuming(least_used_node.ip)
                        else:
                            print(
                                f'{bottleneck_id} Scale up with least used node from queue {most_used_ss_q_name} default_q_size {bottleneck_q_size}')
                            scale_up_node_with_least_used_node(bottleneck_node_obj, least_used_node,
                                                               queue_name=most_used_ss_q_name)
                            # node_API.write_timestamp(ssh_instance)
                            scaled_up = True
                            # LAST_SCALE_UP = datetime.datetime.now()
                    else:
                        pass
                        # print(f'Node {bottleneck_id} No least used node received')
                else:
                    # Assign idle node to network
                    print(f'Node {bottleneck_id} GOT IDLE node, scaling using idle node')
                    assign_node(idle_node, bottleneck_id, bottleneck_ip, bottleneck_function, bottleneck_lo)
                    LAST_SCALE_UP = datetime.datetime.now()
                # print(f'Node {bottleneck_id} RELEASE the mutex 1')
                mutex_self_scale.release()
                if scaled_up:
                    time.sleep(30)
            else:
                pass
                # print(f'Node {bottleneck_id} FAILED the mutex 1')
        time.sleep(2)


def launch_self_scale_check():
    threads = []
    for node in nodes:
        print(f'Launch self scale checker for node {node.id}')
        processThread = threading.Thread(target=self_scale_worker, args=(node,))
        processThread.start()
        threads.append(processThread)
        print('Launch done')
    for thread in threads:
        print(f'Joining thread {thread}')
        thread.join()
    print('Done joining threads')
    # for node in nodes:
    #     print(f'Launching least_used_scaler for node {node.id}')
    #     processThread = threading.Thread(target=least_used_scaler, args=(node,))
    #     processThread.start()
    #     time.sleep(2)
    # print('Done launching least used scaler threads')


def am_busiest_node(me_node_id):
    largest_work = -1
    busiest_node_id = None
    if mutex_am_busiest.acquire(True):
        for node in nodes:
            q_size = node_API.get_q_size(node.ip)
            crt_work_left = compute_remaining_work(node, q_size)
            if crt_work_left > largest_work:
                busiest_node_id = node.id
                largest_work = crt_work_left
        mutex_am_busiest.release()
        if busiest_node_id == me_node_id:
            return True

    return False


def self_scale_worker(input_node_obj, require_mutex=True):
    # bottleneck_id = node.id
    while True:
        # bottleneck_node_obj = get_node(bottleneck_id)
        bottleneck_ip = input_node_obj.ip
        # bottleneck_q_size = node_API.get_q_size(bottleneck_ip)
        bottleneck_function = input_node_obj.function
        bottleneck_lo = '172.16.0.' + input_node_obj.id

        subsequent_function = get_subsequent_function(input_node_obj.last_function)

        print(
            f'----- SELF SCALE WORKER last_used_node {input_node_obj.id} last_function {input_node_obj.last_function} function: {bottleneck_function} subseq_f: {subsequent_function}')
        if input_node_obj.id != '1' and input_node_obj.id != LAST_PIPELINE_NODE and subsequent_function != filter_types.MERGE_PARTS:
            if require_mutex and mutex_self_scale.acquire(True):
                pass
            print(f'Node {input_node_obj.id} GOT the mutex 2')
            self_scale(bottleneck_lo, input_node_obj.ip, input_node_obj.id, input_node_obj)
            print(f'Node {input_node_obj.id} RELEASE the mutex 2')
            if require_mutex:
                mutex_self_scale.release()
        else:
            print(
                f'Node {input_node_obj.id} has nothing left to scale, subsequent function is {subsequent_function}, RETURN ')
            return
            # print(f'Node {bottleneck_id} FAILED the mutex 2')
        # print(f'Node {bottleneck_id} SLEEPS for 8 seconds')
        time.sleep(4)


def launch_producer_processes():
    for node in nodes:
        if node.id != '1' and node.id != '7':
            print(f'Launching producer process for node {node.id}')
            node_API.setup_producer(vm_ip=node.ip, loopback_addr=f'172.16.0.{node.id}', queue_name='first',
                                    folder_name='distributed_files')


def check_for_idle_nodes():
    print('------- Checking for idle nodes')
    idle_nodes = []
    assigned_nodes = []
    scaled_nodes = []
    ssh_instances = []
    first_q_empty_nodes = []
    for node in nodes:
        ssh_instance = node_API.get_ssh_instance(node.ip, '24adna')
        ssh_instances.append(ssh_instance)
    while True:
        current_node_index = 0
        for node in nodes:
            ssh_instance = ssh_instances[current_node_index]
            if node.id != '1' and node.id != LAST_PIPELINE_NODE:
                ffmpeg_idle = node_API.ffmpeg_has_not_ran_in_x_seconds(node.ip, 15, ssh_instance=ssh_instance)
                if ffmpeg_idle and node.id not in idle_nodes and node.id not in assigned_nodes:
                    print(f'------------- Found idle node {node.id}')
                    idle_nodes.append(node.id)
                elif len(idle_nodes) > 0 and not ffmpeg_idle and node.id not in scaled_nodes and node.id not in first_q_empty_nodes:
                    print(f'********* Potential scale at node {node.id}, checking first queue size')
                    first_q_size = node_API.get_first_q_size(node.ip,ssh_instance)
                    print(f'First queue size {first_q_size}')
                    if first_q_size > 0:
                        idle_node_id = idle_nodes[0]
                        idle_node_obj = get_node(idle_node_id)
                        idle_nodes.remove(idle_node_id)
                        print(f'************** Node {node.id} can be scaled with node {idle_node_id}')
                        node_API.setup_consumer(queue_ip=f'172.16.0.{node.id}', vm_ip=idle_node_obj.ip, task_type=filter_types.BLUR, third_argument='first')
                        assigned_nodes.append(idle_node_id)
                        scaled_nodes.append(node.id)
                    else:
                        first_q_empty_nodes.append(node.id)
            current_node_index += 1
        time.sleep(8)


def launch_ffmpeg_checks():
    for node in nodes:
        print(f'Launch ffmpeg check for node {node.id}')
        node_API.launch_ffmpeg_last_run_check(node.ip)


def start_test():
    unit_test_cloud_flower_pipeline.start()

def threaded_client():
    start_pipeline()
    assign_all_idle_nodes_to_1()
    launch_self_scale_check()
    #interconnect_nodes()
    unit_test_cloud_flower_pipeline.distribute_files()
    time.sleep(20)
    launch_producer_processes()
    print('----------------- Starting test')
    processThread = threading.Thread(target=start_test)
    processThread.start()
    # print('------------------------- Waiting 2700 seconds for nodes to start working')
    # time.sleep(2700)
    # launch_ffmpeg_checks()
    # time.sleep(20)
    # check_for_idle_nodes()


def socket_client(socket_connection, thread_nr):
    socket_connection.send(str.encode('(thread:{thread_nr})Welcome to the Server'))
    while True:
        data = socket_connection.recv(2048)
        data = data.decode('utf-8')
        print(f'(thread:{thread_nr})Controller received data: {data}')
        socket_connection.sendall(str.encode('OK'))


def listen_socket():
    ServerSocket = socket.socket()
    # ServerSocket.settimeout(100)
    host = controller_ip
    port = 1235
    ThreadCount = 0
    try:
        ServerSocket.bind((host, port))
    except socket.error as e:
        print(str(e))

    print('Waitiing for a Connection..')
    ServerSocket.listen(5)

    while True:
        ThreadCount += 1
        print('Starting Thread Number: ' + str(ThreadCount))
        Client, address = ServerSocket.accept()
        print('Connected to: ' + address[0] + ':' + str(address[1]))
        _thread.start_new_thread(socket_client, (Client, ThreadCount,))
    # socket_listen()


if __name__ == '__main__':
    global controller_ip
    controller_ip = sys.argv[1]
    # start_initial_pipeline()
    threaded_client()
    # listen_socket()
