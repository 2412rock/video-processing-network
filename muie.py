import write_timestamp, time, os


def ffmpeg_runs():
    os.system('ps -fA | grep ffmpeg >> ffmpeg_runs_output')
    f = open('ffmpeg_runs_output', 'r')
    ffmpeg_runs_output = f.read()
    f.close()
    os.system('rm ffmpeg_runs_output')
    if '-i part' in ffmpeg_runs_output:
        return True
    return False


if __name__ == '__main__':
    write_timestamp.write_time_stamp() # create the file so we dont have to deal with null file
    while True:
        #print('Checking ffmpeg runs')
        if ffmpeg_runs():
            write_timestamp.write_time_stamp()
        time.sleep(0.5)
