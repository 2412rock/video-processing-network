import VMware_API, sys

def turn_off_vms(end):
    # Starts from 1 instead of 0
    #print("Deleting {0} VMs".format(str(end - start)))

    for i in range(1, end+1):
        #print("Power off VM {0}".format(str(i)))
        VMware_API.turn_off_vm(str(i))
        #API_master.delete_vm(str(i))
        #dd_node(vm_ip, '24adna')

if __name__ == '__main__':
    nr_vms = sys.argv[1]
    nr_vms = int(nr_vms)
    print(f'Turning off {nr_vms} vms')
    turn_off_vms(nr_vms)