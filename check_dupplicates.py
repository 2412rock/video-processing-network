import os, filter_types

base_text = 'Wrote file '

# two part files

def func(file_names, total_parts, filter_array):
    dictionary = {}

    current_index = 1
    while current_index <= total_parts:
        for file_name in file_names:
            for filter in filter_array:
                f = open('node_logs.txt', 'r')
                # print(f'Checking {file_name}')
                for line in f.readlines():
                    # print(line)
                    if file_name in line and (base_text + f'part-{current_index}-of-{total_parts}-') in line and filter in line:
                        # print(line)
                        key = file_name + str(current_index) + '_' + filter
                        if key in dictionary:
                            print(f'{key} occurred more than 1 time')
                            dictionary[key] += 1
                        else:
                            # print(f'Added {key}')
                            dictionary[key] = 1
            f.close()
        current_index += 1

file_names = ['Dönence', 'Yolcu', 'CemalÄ±m']
filter_types = [filter_types.BLUR, filter_types.STABILIZE, filter_types.ADD_WATERMARK, filter_types.REVERSE, filter_types.VINTAGE, filter_types.MERGE_PARTS]
func(file_names,2, filter_types)
file_names = ['bmw']
func(file_names,24, filter_types)
