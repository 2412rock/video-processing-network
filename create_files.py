import os, sys, time


if __name__ == '__main__':
    range_from = int(sys.argv[1])
    nr_files = int(sys.argv[2])

    for file_count in range(range_from, nr_files+1):
        os.system(f'touch {file_count}')
        time.sleep(0.1)