import node_API, VMware_API, time, sys
from datetime import datetime

start_time_total = time.time()

VM_PASS = '24adna'
FINAL_FOLDER_ELEMENTS = int(sys.argv[2])

node_1_ip = '10.149.21.1'#VMware_API.turn_on_vm_and_get_ip('1') First pipeline node
merge_parts_node_ip = '10.149.21.4'#VMware_API.turn_on_vm_and_get_ip('6')

nodes_ips = ['172.16.0.2', '172.16.0.3', '172.16.0.4', '172.16.0.5', '172.16.0.6',
             '172.16.0.10', '172.16.0.11', '172.16.0.12', '172.16.0.13', '172.16.0.14',
             '172.16.0.15', '172.16.0.16', '172.16.0.17', '172.16.0.18', '172.16.0.19']

def build_command():
    nr_of_nodes = len(nodes_ips)
    command = f'cd /home/template/code/vu-bsc; python3 distribute_files.py {nr_of_nodes}'
    for node_ip in nodes_ips:
        command += f' {node_ip}'
    return command

def distribute_files():
    node_ssh = node_API.get_ssh_instance(node_1_ip, VM_PASS)
    print('Copying files to to folder files_to_distribute')
    ssh_stdin, ssh_stdout, ssh_stderr = node_ssh.exec_command(
        'cd /home/template/code; cp -r VU-BSC-VIDEO-PARTS/* vu-bsc/files_to_distribute')
    da = ssh_stdout.read().decode('ascii').strip("\n")

    print('Distributing files to all parallel pipelines')
    command = build_command()
    print(f'Running command {command}')
    ssh_stdin, ssh_stdout, ssh_stderr = node_ssh.exec_command(command)
    da = ssh_stdout.read().decode('utf-8').strip("\n")
    print(da)
    node_ssh.close()

def run_test(node_ssh, node_6_ssh ,test_nr):
    print(f'--------------------------- RUNNING TEST {test_nr} ---------------------------')


    current_count = -1

    print('Ready to test')
    while True:
        ssh_stdin, ssh_stdout, ssh_stderr = node_6_ssh.exec_command(
            'cd /home/template/code/vu-bsc/final_folder; ls | wc -l')
        final_folder_count = ssh_stdout.read().decode('ascii').strip("\n")
        final_folder_count = int(final_folder_count)

        if final_folder_count == FINAL_FOLDER_ELEMENTS:
            print(f'PASSED TEST {test_nr}')
            return
        else:
            if final_folder_count > current_count:
                current_count = final_folder_count
                print(f'COUNT HAS INCREASED {current_count}')
            time.sleep(1)

def start():
    node_1_ssh = node_API.get_ssh_instance(node_1_ip, VM_PASS)
    node_6_ssh = node_API.get_ssh_instance(merge_parts_node_ip, VM_PASS)
    start_time = time.time()
    run_test(node_1_ssh, node_6_ssh,'1')
    print("--- %s minutes ---" % ((time.time() - start_time)/60))
    print('=============================================================')

    node_1_ssh.close()
    node_6_ssh.close()

    print(f'ALL TESTS HAVE PASSED')
    print("--- %s minutes ---" % ((time.time() - start_time_total)/60))
