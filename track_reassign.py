import sys
from datetime import datetime

def track_switch(old_task, new_task, from_q, start_or_stop):
    tracker_file = open('track_time.txt', 'a')
    tracker_file.write(f"{start_or_stop} > {get_now_time()} > SWITCH FROM {old_task} TO {new_task} > from Q {from_q}\n")
    tracker_file.flush()
    tracker_file.close()

def get_now_time():
    now = datetime.now()

    # dd/mm/YY H:M:S
    dt_string = now.strftime("%H:%M:%S")
    return dt_string


if __name__ == '__main__':
    old_task = sys.argv[1]
    new_task = sys.argv[2]
    from_q = sys.argv[3]
    start_or_stop = sys.argv[4]
    track_switch(old_task, new_task, from_q, start_or_stop=start_or_stop)