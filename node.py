class Node:

    def __init__(self, id, func, ip, port):
        self.id = id
        self.function = func
        self.ip = ip
        self.las_used_port = port
        self.nr_elements_q = 0
        self.connections = []
        self.ffmpeg_still_runs = False
        self.current_queue_index = 1
        self.last_function = func

