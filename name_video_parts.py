import os
import sys

unique_file_name = sys.argv[1]

parts_list = []


def get_total_parts():
    crt_index = 0
    for file_name in os.listdir():
        if unique_file_name in file_name:
            crt_index += 1
    return crt_index


def rename_all_files_with_unique_name(total_nr_parts):
    current_index = 1
    for file_name in os.listdir():
        if unique_file_name in file_name:
            new_filename = f"part-{current_index}-of-{total_nr_parts}-startFilename{unique_file_name}.mp4"
            os.rename(file_name, f'{new_filename}')
            current_index += 1


total_parts = get_total_parts()
rename_all_files_with_unique_name(total_parts)


