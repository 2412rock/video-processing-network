class Connection:

    def __init__(self, w_id, subnet, interface_name, is_consumer, q_names=['hello']):
        self.is_consumer = is_consumer
        self.with_node_id = w_id
        self.subnet = subnet
        self.interface_name = interface_name
        self.q_names = q_names
