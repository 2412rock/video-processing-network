import datetime, sys

def write_time_stamp():
    now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    f = open('last_ffmpeg_run', 'w')
    f.write(str(now))
    f.close()

try:
    sys.argv[1]
    write_time_stamp()
except:
    pass