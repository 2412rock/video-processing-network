'''
Requirements:
Python3.7+
pip3 install pandas==1.4.2 (latest)
pip3 install matplotlib==3.5.2 (latest)
'''

import datetime
from fileinput import filename

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

def parser(read_path="./node_logs.txt"):
    '''
    Input: read_path = path of node logs txt file
    Input type: str
    
    Output: PNG file with desired graph
    Output type: .png
    '''

    raw_file = open(read_path, 'r').readlines()
    df = pd.DataFrame(data={'Node': [], 'Start': [], 'End': [], 'FilePart':[]})

    mintime = datetime.datetime(year=3022, month=1, day=1, hour=1, minute=1, second=1)
    
    for line in raw_file:
        if line.startswith("START"):
            if datetime.datetime(year = 2022, month = 5, day = 15, hour  = int(line.split(">")[1].split(":")[0]), minute =  int(line.split(">")[1].split(":")[1]), second = int(line.split(">")[1].split(":")[2])) < mintime:
                mintime = datetime.datetime(
                                            year = 2022,
                                            month = 5,
                                            day = 15,
                                            hour = int(line.split(">")[1].split(":")[0]),
                                            minute = int(line.split(">")[1].split(":")[1]),
                                            second = int(line.split(">")[1].split(":")[2])
                                         )
    #print(mintime)
 
    for line in raw_file:
        if line.startswith("---"):
            current_node = int(line.split(' ')[2])

        if line.startswith("START"):
            line_df = pd.DataFrame(data={'Node': [current_node],
                                         'Start': datetime.datetime(
                                            year = 2022,
                                            month = 5,
                                            day = 15,
                                            hour = int(line.split(">")[1].split(":")[0]),
                                            minute = int(line.split(">")[1].split(":")[1]),
                                            second = int(line.split(">")[1].split(":")[2])
                                         )-datetime.timedelta(days=mintime.day, hours=mintime.hour, minutes=mintime.minute, seconds=mintime.second)+datetime.timedelta(seconds=10),
                                         'End': ["Placeholder"],
                                         'FilePart':'SWITCH' if 'SWITCH' in line else ''.join([line.split(">")[2].split("-")[1],'/',line.split(">")[2].split("-")[3], line.split(">")[-1]]),
                                         'FileName':'SWITCH' if 'SWITCH' in line else line.split(">")[2]
                                        })
            df = pd.concat([df, line_df])

            #if line_df['Start'] < mintime:
            #    mintime = line_df['Start']

        if line.startswith("END"):
            df.iloc[-1, df.columns.get_loc('End')] = datetime.datetime(
                                            year = 2022,
                                            month = 5,
                                            day = 15,
                                            hour = int(line.split(">")[1].split(":")[0]),
                                            minute = int(line.split(">")[1].split(":")[1]),
                                            second = int(line.split(">")[1].split(":")[2])
                                            )-datetime.timedelta(days=mintime.day, hours=mintime.hour, minutes=mintime.minute, seconds=mintime.second)+datetime.timedelta(seconds=10)

    #print(mintime)

    for index, row in df.iterrows():

        color = ''
        filename = ''
        if row['FileName']=="SWITCH":
            color = 'red'
        elif 'Filenamebmw' in row["FileName"]:
            filename = 'FILE_1'
            color = 'blue'
        elif 'FilenameYol' in row["FileName"]:
            filename = 'FILE_2'
            color = 'orange'
        elif 'FilenameCem' in row["FileName"]:
            filename = 'FILE_3'
            color = 'purple'
        else:
            filename = 'FILE_4'
            color = 'green'

        plt.plot(
                (row["Start"],row["End"]), 
                (int(row["Node"]), int(row["Node"])),
                color=color,
                linewidth=12 # THIS IS WHERE YOU CONTROL THE LINE THICKNESS
                )
        # if 'bmw' in row["FileName"] and '23/24' in  row["FilePart"]:
        #print('FILEname '+ filename)
        plt.text(row["Start"], row["Node"], row["FilePart"][0:8], size='x-small', color='white', weight='bold', verticalalignment="center")


    plt.yticks(df['Node'].tolist())
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%M:%S'))

    plt.title("Running time 14.50 mins")

    plt.savefig('graph.png')
    plt.show()

    return

if __name__ == "__main__":
    parser()
