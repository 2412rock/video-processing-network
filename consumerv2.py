import functools
import logging
import pika
import threading
import time
import sys
import json, base64, subprocess, shutil, filter_types, os

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)

logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)


def process(filename, task_type):
    # print(f'Processing filename: {filename}')
    time.sleep(5)
    if task_type == filter_types.BLUR:
        os.system(
            f'ffmpeg -i "{filename}" \-filter_complex "[0:v]crop=200:400:300:350,boxblur=10[fg]; [0:v][fg]overlay=300:350[v]" \-map "[v]"  \ blurredVideo_{filename}.mp4')
        shutil.move(f' blurredVideo_{filename}.mp4', 'producer_folder/' + filename)
    elif task_type == filter_types.STABILIZE:
        # Replace with actual encoding
        os.system(
            f'ffmpeg -i "{filename}" \-filter_complex "[0:v]crop=200:400:300:350,boxblur=10[fg]; [0:v][fg]overlay=300:350[v]" \-map "[v]"  \ blurredVideo_{filename}.mp4')
        shutil.move(f' blurredVideo_{filename}.mp4', 'producer_folder/' + filename)
    elif task_type == filter_types.MERGE_PARTS:
        # Replace with actual encoding
        # os.system(
        #  f'ffmpeg -i "{filename}" \-filter_complex "[0:v]crop=200:400:300:350,boxblur=10[fg]; [0:v][fg]overlay=300:350[v]" \-map "[v]"  \ blurredVideo_{filename}.mp4')
        shutil.move(filename, 'producer_folder/' + filename)


def get_video_length(filename):
    result = subprocess.run(["ffprobe", "-v", "error", "-show_entries",
                             "format=duration", "-of",
                             "default=noprint_wrappers=1:nokey=1", filename],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)
    return float(result.stdout)


def split_video(filename):
    # splits video in multiple parts and saves them in output folder to be read by the producer
    # print(f'Splitting video {filename}')
    # sas split only half the vier
    # save output to producer folder
    # shutil.move(os.curdir+'/'+filename, '../producer_folder/'+filename)=

    v_len = get_video_length(filename)
    v_len_int = int(v_len)
    v_len_int = v_len_int / 60  # minutes
    # We want to split in chunks of 2 minutes each
    number_of_chunks = v_len_int
    number_of_chunks = int(round(number_of_chunks))
    # print(f'Number of chunks {number_of_chunks}')
    # print(f'vid len: {v_len}')
    start = 0
    end = 0
    for chunk in range(1, number_of_chunks + 1):
        # #print(f'------start:{start} end:{end}')
        start = end
        end += 60
        if end > v_len:
            end = v_len
        # print(f'Splitting {start}:{end}')
        part_name = f'part_{chunk}' + '_of_' + str(number_of_chunks) + '_startFilename' + filename
        os.system(f'ffmpeg -i "{filename}" -ss {start} -t 60 "{part_name}"')
        shutil.move(part_name, '../producer_folder/' + part_name)


def ffmpeg_running():
    name = 'ffmpeg'
    for line in os.popen("ps ax | grep " + name + " | grep -v grep"):
        return True
    return False


def ack_message(channel, delivery_tag, ack):
    """Note that `channel` must be the same pika channel instance via which
    the message being ACKed was retrieved (AMQP protocol constraint).
    """
    if ack:
        channel.basic_ack(delivery_tag)
    else:
        # Channel is already closed, so we can't ACK this message;
        # log and/or do something that makes sense for your app in this case.
        channel.basic_nack(delivery_tag=delivery_tag, multiple=False, requeue=True)
        exit(1)


def do_work(connection, channel, delivery_tag, body):
    thread_id = threading.get_ident()

    d = json.loads(body.decode())
    file_name = d.pop('file_name')
    ack = False
    if file_name == 'END_OF_CONN':
        # channel.basic_ack(delivery_tag=delivery_tag)
        ack = True
    elif not ffmpeg_running():
        # print(f'Received file name {file_name}')
        ack = True
        data = d.pop('binary')
        data = base64.b64decode(data)

        f = open(file_name, 'wb')
        f.write(data)
        f.close()

        process(file_name, task_type)

        try:
            os.remove(file_name)
        except:
            pass
        # print('Removed local file')
        # ch.basic_ack(delivery_tag=method.delivery_tag)
        # print('Sent ack')
    else:
        # print('Rejected message because ffmpeg is running')
        # ch.basic_nack(delivery_tag=method.delivery_tag, multiple=False, requeue=True)
        ack = False
    # time.sleep(10)
    if ack:
        channel.basic_ack(delivery_tag)
    else:
        # Channel is already closed, so we can't ACK this message;
        # log and/or do something that makes sense for your app in this case.
        channel.basic_nack(delivery_tag=delivery_tag, multiple=False, requeue=True)
        exit(1)


def on_message(channel, method_frame, header_frame, body, args):
    (connection, threads) = args
    delivery_tag = method_frame.delivery_tag
    t = threading.Thread(target=do_work, args=(connection, channel, delivery_tag, body))
    t.start()
    threads.append(t)


# Note: sending a short heartbeat to prove that heartbeats are still
# sent even though the worker simulates long-running work

if __name__ == '__main__':
    global task_type
    task_type = sys.argv[1]
    queue_ip = sys.argv[2]

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=queue_ip, credentials=pika.PlainCredentials('adi', '24adna'),
                                  heartbeat=5))  # loopback adress of above node
    channel = connection.channel()

    channel.queue_declare(queue="hello")

    channel.basic_qos(prefetch_count=1)

    threads = []
    on_message_callback = functools.partial(on_message, args=(connection, threads))
    channel.basic_consume(on_message_callback= on_message_callback, queue='hello')

    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()

    # Wait for all to complete
    for thread in threads:
        thread.join()

    connection.close()
