#installs wireguard and other necessary stuff
sudo apt-get update && sudo apt-get upgrade -y
sudo apt-get install wireguard -y
sudo apt-get install git -y
sudo apt-get install openssh-server -y
sudo apt install ffmpeg -y
sudo apt install python3-pip -y

sudo pip3 install pika pytube psutil natsort
# Set PermitRootLogin yes
# dont forget to set sudo password to ssh as root, sudo su; passwd
sudo systemctl enable ssh
sudo systemctl start ssh
sudo sysctl -w net.ipv4.ip_forward=1
sudo sysctl -p

apt install net-tools -y
sudo apt install openresolv -y

#installs and setups quagga and others
sudo apt-get install quagga telnet -y
sudo su - -c "cat << EOF > /etc/quagga/daemons
zebra=yes
ospfd=yes
EOF
"
sudo su - -c "cat << EOF > /etc/quagga/daemons.conf
zebra=yes
ospfd=yes
EOF
"
sudo sysctl -w net.ipv4.conf.all.forwarding=1
sudo sysctl -w net.ipv4.ipfrag_secret_interval=1 # XXX: is this important?

# allow packet coming from one interface to go out on another
sudo sysctl -w net.ipv4.conf.all.rp_filter=0
sudo sysctl -w net.ipv4.conf.default.rp_filter=0

# Empty the cache and don't allow the cache to be made
# XXX: does it really help?
sudo sysctl -w net.ipv4.route.max_size=0
sudo sysctl -w net.ipv4.route.flush=1

# set loopback address based in the nid

#sudo ip addr flush dev lo
#sudo ip addr add 10.0.0.1/32 dev lo
#sudo ip addr add 127.0.0.1/8 dev lo

sudo su - -c "cat <<EOF > /etc/quagga/zebra.conf
password 1234
enable password 1234
line vty
hostname `hostname`
EOF
"


sudo /etc/init.d/zebra restart
sudo /etc/init.d/ospfd restart

