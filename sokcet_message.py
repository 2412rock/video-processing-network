import json

class SocketMessage:

    def __init__(self, data):
        data_json = json.loads(data)
        self.bottleneck_q_size = int(data_json['q_size'])
        self.bottleneck_id = data_json['id']
        self.bottleneck_function = get_node_function(bottleneck_id)
        self.bottleneck_ip = get_node_ip(bottleneck_id)
        self.bottleneck_lo = '172.16.0.' + bottleneck_id