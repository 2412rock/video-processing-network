import os, subprocess
import time


def launch_process(line):
    words = line.split()
    arr = []
    for word in words:
        arr.append(word)
    # print(f'Array consumer : {arr}')
    subprocess.Popen(arr)



def checker():
    cmd = subprocess.check_output("ps -fA | grep python3", shell=True);
    # print (output)
    running_python_processes = cmd.decode('utf-8')

    try:
        # multiple instances of producer, not just one
        f = open('producer.txt', 'r')
        lines = f.readlines()
        for line in lines:
            if line not in running_python_processes: # line not already running
                # producer process for some lo not active
                # print('producer process not running')
                launch_process(line)
    except:
        pass
    # print(output)
    # print(output)


if __name__ == '__main__':
    try:
        while True:
            checker()
            time.sleep(5)
    except Exception as e:
        f = open('checker_daemon_except', 'a')
        f.write(e)
        f.flush()
        f.close()
