import os

if __name__ == '__main__':
    files = os.listdir('/etc/wireguard')
    for file in files:
        file = file.replace('.conf', '')
        print(f'bring down {file}')
        os.system(f'wg-quick down {file}')
