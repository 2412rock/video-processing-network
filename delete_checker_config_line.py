import sys

if __name__ == '__main__':
    line_to_remove = sys.argv[1]
    with open("consumer.txt", "r") as f:
        lines = f.readlines()
    with open("consumer.txt", "w") as f:
        for line in lines:
            if line_to_remove not in line.strip("\n"):
                f.write(line)