import pika, json, base64, shutil, os, sys

TIME_PER_MEGABYTE = 5  # seconds


class Process:

    def __init__(self):
        self.processed_files = []
        self.total_bytes = 0
        self.nr_of_videos = 0

    def callback(self, ch, method, properties, body):
        d = json.loads(body.decode())
        print('---------------------------')
        print(self.processed_files)
        file_name = d.pop('file_name')
        # print(f'Received file name {file_name}')

        if file_name in self.processed_files:
            # Am luat-o de la capat
            print(f'AM LUAT-O DE LA CAPAT {file_name}')
            # WRITE WORK LEFT IN FILE THAT WILL BE READ BY SOCKET CLIENT
            total_in_megabytes = self.total_bytes / 1000000
            f = open('checker_folder/work_left.txt', 'w')
            work_left = total_in_megabytes * TIME_PER_MEGABYTE * self.nr_of_videos
            f.write(str(work_left))
            f.close()

            data = d.pop('binary')
            data = base64.b64decode(data)
            f = open(f'checker_folder/{file_name}', 'wb')
            f.write(data)
            f.close()

            # RESET VALUES
            size_bytes = os.path.getsize(f'checker_folder/{file_name}')
            self.processed_files = []
            self.processed_files.append(file_name)
            self.total_bytes = size_bytes
            self.nr_of_videos = 1

            shutil.move(f'checker_folder/{file_name}', f'producer_folder/{file_name}')
            ch.basic_ack(delivery_tag=method.delivery_tag)

        else:
            print(f'Got file {file_name}')
            data = d.pop('binary')
            data = base64.b64decode(data)
            f = open(f'checker_folder/{file_name}', 'wb')
            f.write(data)
            f.close()

            size_bytes = os.path.getsize(f'checker_folder/{file_name}')
            self.nr_of_videos += 1
            self.total_bytes += size_bytes
            self.processed_files.append(file_name)

            shutil.move(f'checker_folder/{file_name}', f'producer_folder/{file_name}')
            ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == '__main__':
    ip_addr = sys.argv[1]
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=ip_addr, credentials=pika.PlainCredentials('adi', '24adna'),
                                  heartbeat=120))  # loopback adress of above node
    channel = connection.channel()

    channel.queue_declare(queue='hello')

    processObj = Process()

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue='hello', on_message_callback=processObj.callback)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()
