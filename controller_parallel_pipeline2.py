import time
import json, sys, time

import get_queue_size
import VMware_API, _thread, threading, connection, node, filter_types, VMware_API
import node_API_parallel_pipeline as node_API, unit_test_cloud_parallel_pipeline

test_var = 0
nodes = []
mutex = threading.Lock()
freshly_assigned_nodes = {}
freshly_scaled_nodes = {}

SCALE_UP_THRESHOLD_VALUE = 60  # amount of seconds left until work is done
MINIMUM_MOST_BUSY_THRESHOLD = 20
SPLIT_NODE_LO = '172.16.0.1'
BLUR_NODE_LO = '172.16.0.2'
STABILIZE_NODE_LO = '172.16.0.3'
ADD_WATERMARK_NODE_LO = '172.16.0.4'
REVERSE_NODE_LO = '172.16.0.5'
VINTAGE_NODE_LO = '172.16.0.6'
MERGE_PARTS_NODE_LO = '172.16.0.7'

FRESHLY_ASSINED_NODE_SKIP_COUNT = 14
SLEEP_BETWEEN_CHECKS = 10
FRESHLY_SCALED_NODES_SKIP = 0
PENULTIM_PIPELINE_NODE = '6'
PENULTIM_PIPELINE_FUNCTION = filter_types.VINTAGE
PIPELINE_NODES = []
LAST_PIPELINE_NODE = '7'

pipeline_nodes = ['1', '2', '3', '4', '5', '6', '7']

MAX_NR_CONSUMERS = 4


def set_last_used_port(node_id, port):
    get_node(node_id).las_used_port = port


def get_node_last_used_port(node_id):
    return get_node(node_id).las_used_port


def get_node_ip(node_id):
    return get_node(node_id).ip


def get_node_function(node_id):
    return get_node(node_id).function


def get_idle_node():
    for node in nodes:
        if node.function == 'IDLE':
            return node
    return None


def add_node(node):
    nodes.append(node)


def get_node(id):
    for node in nodes:
        if node.id == id:
            return node


def get_subnets_of_node(id):
    # TO DO
    # returns all subnets for now because I suspect a network conflict
    subnets = []
    for node in nodes:
        connections = node.connections
        for connection in connections:
            # print(f' (iiiiiiiiiiiiiiiiiiiiii) subnet of node id {id} {connection.subnet}')
            subnets.append(connection.subnet)

    return subnets


def get_next_subnet(subnets):
    digit = '1'
    current = f'10.0.{digit}.x'

    while current in subnets:
        digit = int(digit) + 1
        digit = str(digit)
        current = f'10.0.{digit}.x'
    return current


def get_subsequent_function(current_func):
    if current_func == filter_types.SPLIT:
        return filter_types.BLUR
    elif current_func == filter_types.BLUR:
        return filter_types.STABILIZE
    elif current_func == filter_types.STABILIZE:
        return filter_types.ADD_WATERMARK
    elif current_func == filter_types.ADD_WATERMARK:
        return filter_types.REVERSE
    elif current_func == filter_types.REVERSE:
        return filter_types.VINTAGE
    elif current_func == filter_types.VINTAGE:
        return filter_types.MERGE_PARTS
    elif current_func == filter_types.MERGE_PARTS:
        return filter_types.MERGE_PARTS


def get_subsequent_nodes_of(node_function, id, func):
    sub_fun = get_subsequent_function(node_function)
    result = []
    if func == filter_types.MERGE_PARTS:
        # If there are multiple end of the line nodes, only send to the static one
        print('********************* RETURNING ONLY NODE 6')
        result.append(get_node(LAST_PIPELINE_NODE))
        return result

    if sub_fun is not None:
        for node in nodes:
            if node.function == sub_fun and node.id != id:  # prevent connecting to itself
                result.append(node)
    return result


def reset_node_and_neighbours_connection(node_id):
    node = get_node(node_id)
    for connection in node.connections:
        w_id = connection.with_node_id
        w_node = get_node(w_id)
        connection_counter = -1
        for w_node_conn in w_node.connections:
            connection_counter += 1
            if w_node_conn.with_node_id == node_id:
                break

        w_node.connections.pop(connection_counter)


def init_idle_node(vm_ip):
    node_API.run_initial_setup(vm_ip)
    node_API.run_pip_installers(vm_ip)
    node_API.run_rabbitmq_setup(vm_ip)
    node_API.reset_wireguard_and_zebra(vm_ip)


def boot_and_init_idle_nodes():
    print(
        '-------------------------------------------------------------------------------------------------------------> Initialising idle nodes')
    # setup_node_ip = VMware_API.turn_on_vm_and_get_ip('setup_vm')
    # node_API.reset_wireguard_and_zebra(setup_node_ip)
    threads = []

    idle_node_1_ip = '10.149.21.7'  # VMware_API.turn_on_vm_and_get_ip('10')
    # node_API.run_initial_setup(idle_node_1_ip)
    # node_API.run_pip_installers(idle_node_1_ip)
    # node_API.run_rabbitmq_setup(idle_node_1_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_1_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_1_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_2_ip = '10.149.21.8'  # VMware_API.turn_on_vm_and_get_ip('11')
    # node_API.run_initial_setup(idle_node_2_ip)
    # node_API.run_pip_installers(idle_node_2_ip)
    # node_API.run_rabbitmq_setup(idle_node_2_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_2_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_2_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_3_ip = '10.149.21.9'  # VMware_API.turn_on_vm_and_get_ip('12')
    # node_API.run_initial_setup(idle_node_3_ip)
    # node_API.run_pip_installers(idle_node_3_ip)
    # node_API.run_rabbitmq_setup(idle_node_3_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_3_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_3_ip,))
    processThread.start()
    threads.append(processThread)
    #
    idle_node_4_ip = '10.149.21.10'  # VMware_API.turn_on_vm_and_get_ip('13')
    # node_API.run_initial_setup(idle_node_4_ip)
    # node_API.run_pip_installers(idle_node_4_ip)
    # node_API.run_rabbitmq_setup(idle_node_4_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_4_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_4_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_5_ip = '10.149.21.11'  # VMware_API.turn_on_vm_and_get_ip('14')
    # node_API.run_initial_setup(idle_node_5_ip)
    # node_API.run_pip_installers(idle_node_5_ip)
    # node_API.run_rabbitmq_setup(idle_node_5_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_5_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_5_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_6_ip = '10.149.21.12'  # VMware_API.turn_on_vm_and_get_ip('15')
    # node_API.run_initial_setup(idle_node_6_ip)
    # node_API.run_pip_installers(idle_node_6_ip)
    # node_API.run_rabbitmq_setup(idle_node_6_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_6_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_6_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_7_ip = '10.149.21.13'  # VMware_API.turn_on_vm_and_get_ip('16')
    # node_API.run_initial_setup(idle_node_7_ip)
    # node_API.run_pip_installers(idle_node_7_ip)
    # node_API.run_rabbitmq_setup(idle_node_7_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_7_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_7_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_8_ip = '10.149.21.14'  # VMware_API.turn_on_vm_and_get_ip('17')
    # node_API.run_initial_setup(idle_node_8_ip)
    # node_API.run_pip_installers(idle_node_8_ip)
    # node_API.run_rabbitmq_setup(idle_node_8_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_8_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_8_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_9_ip = '10.149.21.15'  # VMware_API.turn_on_vm_and_get_ip('18')
    # node_API.run_initial_setup(idle_node_9_ip)
    # node_API.run_pip_installers(idle_node_9_ip)
    # node_API.run_rabbitmq_setup(idle_node_9_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_9_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_9_ip,))
    processThread.start()
    threads.append(processThread)

    idle_node_10_ip = '10.149.21.16'  # VMware_API.turn_on_vm_and_get_ip('19')
    # node_API.run_initial_setup(idle_node_10_ip)
    # node_API.run_pip_installers(idle_node_10_ip)
    # node_API.run_rabbitmq_setup(idle_node_10_ip)
    # node_API.reset_wireguard_and_zebra(idle_node_10_ip)
    processThread = threading.Thread(target=init_idle_node, args=(idle_node_10_ip,))
    processThread.start()
    threads.append(processThread)

    for thread in threads:
        print(f'Joining thread {thread}')
        thread.join()

    add_node(node.Node('10', 'IDLE', idle_node_1_ip, '51820'))
    add_node(node.Node('11', 'IDLE', idle_node_2_ip, '51820'))
    add_node(node.Node('12', 'IDLE', idle_node_3_ip, '51820'))
    add_node(node.Node('13', 'IDLE', idle_node_4_ip, '51820'))
    add_node(node.Node('14', 'IDLE', idle_node_5_ip, '51820'))
    add_node(node.Node('15', 'IDLE', idle_node_6_ip, '51820'))
    add_node(node.Node('16', 'IDLE', idle_node_7_ip, '51820'))
    add_node(node.Node('17', 'IDLE', idle_node_8_ip, '51820'))
    add_node(node.Node('18', 'IDLE', idle_node_9_ip, '51820'))
    add_node(node.Node('19', 'IDLE', idle_node_10_ip, '51820'))

    node_API.set_id(idle_node_1_ip, '10')
    node_API.set_id(idle_node_2_ip, '11')
    node_API.set_id(idle_node_3_ip, '12')
    node_API.set_id(idle_node_4_ip, '13')
    node_API.set_id(idle_node_5_ip, '14')
    node_API.set_id(idle_node_6_ip, '15')
    node_API.set_id(idle_node_7_ip, '16')
    node_API.set_id(idle_node_8_ip, '17')
    node_API.set_id(idle_node_9_ip, '18')
    node_API.set_id(idle_node_10_ip, '19')


def boot_and_init_setup_pipeline_nodes():
    threads = []
    print(
        f'------------------------------------------------------------------------------------------------------------> Starting pipeline nodes')
    split_node_ip = '10.149.21.1'  # VMware_API.turn_on_vm_and_get_ip('1')
    processThread = threading.Thread(target=init_idle_node, args=(split_node_ip,))
    processThread.start()
    threads.append(processThread)

    blur_node_ip = '10.149.21.2'  # VMware_API.turn_on_vm_and_get_ip('2')
    processThread = threading.Thread(target=init_idle_node, args=(blur_node_ip,))
    processThread.start()
    threads.append(processThread)

    stabilize_video_node_ip = '10.149.21.3'  # VMware_API.turn_on_vm_and_get_ip('3')
    processThread = threading.Thread(target=init_idle_node, args=(stabilize_video_node_ip,))
    processThread.start()
    threads.append(processThread)

    add_watermark_node_ip = '10.149.21.5'  # VMware_API.turn_on_vm_and_get_ip('3')
    processThread = threading.Thread(target=init_idle_node, args=(add_watermark_node_ip,))
    processThread.start()
    threads.append(processThread)

    reverse_node_ip = '10.149.21.18'  # VMware_API.turn_on_vm_and_get_ip('3')
    processThread = threading.Thread(target=init_idle_node, args=(reverse_node_ip,))
    processThread.start()
    threads.append(processThread)

    vintage_node_ip = '10.149.21.19'  # VMware_API.turn_on_vm_and_get_ip('3')
    processThread = threading.Thread(target=init_idle_node, args=(vintage_node_ip,))
    processThread.start()
    threads.append(processThread)

    merge_parts_node_ip = '10.149.21.4'  # VMware_API.turn_on_vm_and_get_ip('6')
    processThread = threading.Thread(target=init_idle_node, args=(merge_parts_node_ip,))
    processThread.start()
    threads.append(processThread)

    for thread in threads:
        print(f'Joining thread {thread}')
        thread.join()

    add_node(node.Node('1', filter_types.SPLIT, split_node_ip, '51820'))
    add_node(node.Node('2', filter_types.BLUR, blur_node_ip, '51820'))
    add_node(node.Node('3', filter_types.STABILIZE, stabilize_video_node_ip, '51820'))
    add_node(node.Node('4', filter_types.ADD_WATERMARK, add_watermark_node_ip, '51820'))
    add_node(node.Node('5', filter_types.REVERSE, reverse_node_ip, '51820'))
    add_node(node.Node('6', filter_types.VINTAGE, vintage_node_ip, '51820'))
    add_node(node.Node('7', filter_types.MERGE_PARTS, merge_parts_node_ip, '51820'))

    node_API.set_id(split_node_ip, '1')
    node_API.set_id(blur_node_ip, '2')
    node_API.set_id(stabilize_video_node_ip, '3')
    node_API.set_id(add_watermark_node_ip, '4')
    node_API.set_id(reverse_node_ip, '5')
    node_API.set_id(vintage_node_ip, '6')
    node_API.set_id(merge_parts_node_ip, '7')

    return split_node_ip, blur_node_ip, stabilize_video_node_ip, add_watermark_node_ip, reverse_node_ip, vintage_node_ip, merge_parts_node_ip


def save_connection(id_1, id_2, interface_name_1, interface_name_2, subnet):
    # print(f'SAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE {}')
    node_1 = get_node(id_1)
    node_1.connections.append(
        connection.Connection(id_2, subnet, interface_name_1, is_consumer=False))  # connection with node 2

    node_2 = get_node(id_2)
    node_2.connections.append(connection.Connection(id_1, subnet, interface_name_2, is_consumer=True))

    # print(
    # f' (iiiiiiiiiiiiiiiiiiiiiiiiii) Saving connection {id_1} -> {id_2}, interface_1 {interface_name_1}, interface_2: {interface_name_2},  subnet: {subnet}')


def start_initial_pipeline():
    split_node_ip, blur_node_ip, stabilize_node_ip, add_watermark_node_ip, reverse_node_ip, vintage_node_ip, merge_parts_node_ip = boot_and_init_setup_pipeline_nodes()
    boot_and_init_idle_nodes()

    # Setup the default

    ################################################################################################
    interface_1, interface_2 = node_API.connect_nodes(split_node_ip, blur_node_ip, '1', '2', '51820', '24adna',
                                                      '10.0.1.')
    save_connection('1', '2', interface_1, interface_2, '10.0.1.x')

    node_API.launch_checker_daemon(split_node_ip)
    #node_API.launch_ffmpeg_last_run_check(split_node_ip)
    # node_API.setup_consumer('None', split_node_ip, filter_types.SPLIT)
    # node_API.setup_producer(split_node_ip, SPLIT_NODE_LO)
    # node_API.start_socket(split_node_ip, controller_ip)

    node_API.launch_checker_daemon(blur_node_ip)
    #node_API.launch_ffmpeg_last_run_check(blur_node_ip)
    #node_API.setup_producer(vm_ip=blur_node_ip, loopback_addr=BLUR_NODE_LO, queue_name='first', folder_name='distributed_files')
    node_API.setup_consumer(BLUR_NODE_LO, blur_node_ip,
                            filter_types.BLUR, third_argument='first')  # split_node_ip should be replaced with loopback
    node_API.setup_producer(blur_node_ip, BLUR_NODE_LO)
    node_API.launch_receive_files_process(vm_ip=blur_node_ip, host_ip=BLUR_NODE_LO)
    node_API.start_socket(blur_node_ip, controller_ip)
    ################################################################################################ CONNECT BLUR NODE TO STABILIZE
    interface_1, interface_2 = node_API.connect_nodes(blur_node_ip, stabilize_node_ip, '2', '3', '51820', '24adna',
                                                      '10.0.2.')
    save_connection('2', '3', interface_1, interface_2, '10.0.2.x')

    node_API.launch_checker_daemon(stabilize_node_ip)
    #node_API.launch_ffmpeg_last_run_check(stabilize_node_ip)
    node_API.setup_consumer(BLUR_NODE_LO, stabilize_node_ip, filter_types.STABILIZE)
    node_API.setup_producer(stabilize_node_ip, STABILIZE_NODE_LO)
    node_API.start_socket(stabilize_node_ip, controller_ip)
    ################################################################################################ CONNECT STABILIZE TO ADD_WATERMARK
    interface_1, interface_2 = node_API.connect_nodes(stabilize_node_ip, add_watermark_node_ip, '3', '4', '51820',
                                                      '24adna',
                                                      '10.0.3.')
    save_connection('3', '4', interface_1, interface_2, '10.0.3.x')

    node_API.launch_checker_daemon(add_watermark_node_ip)
    #node_API.launch_ffmpeg_last_run_check(add_watermark_node_ip)
    node_API.setup_consumer(STABILIZE_NODE_LO, add_watermark_node_ip, filter_types.ADD_WATERMARK)
    node_API.setup_producer(add_watermark_node_ip, ADD_WATERMARK_NODE_LO)
    node_API.start_socket(add_watermark_node_ip, controller_ip)
    ################################################################################################ CONNECT ADD_WATERMARK TO REVERSE
    interface_1, interface_2 = node_API.connect_nodes(add_watermark_node_ip, reverse_node_ip, '4', '5', '51820',
                                                      '24adna',
                                                      '10.0.4.')
    save_connection('4', '5', interface_1, interface_2, '10.0.4.x')

    node_API.launch_checker_daemon(reverse_node_ip)
   # node_API.launch_ffmpeg_last_run_check(reverse_node_ip)
    node_API.setup_consumer(ADD_WATERMARK_NODE_LO, reverse_node_ip, filter_types.REVERSE)
    node_API.setup_producer(reverse_node_ip, REVERSE_NODE_LO)
    node_API.start_socket(reverse_node_ip, controller_ip)
    ################################################################################################ CONNECT ADD_WATERMARK TO REVERSE
    interface_1, interface_2 = node_API.connect_nodes(reverse_node_ip, vintage_node_ip, '5', '6', '51820',
                                                      '24adna',
                                                      '10.0.5.')
    save_connection('5', '6', interface_1, interface_2, '10.0.5.x')

    node_API.launch_checker_daemon(vintage_node_ip)
    #node_API.launch_ffmpeg_last_run_check(vintage_node_ip)
    node_API.setup_consumer(REVERSE_NODE_LO, vintage_node_ip, filter_types.VINTAGE)
    node_API.setup_producer(vintage_node_ip, VINTAGE_NODE_LO)
    node_API.start_socket(vintage_node_ip, controller_ip)
    ################################################################################################ CONNECT ADD_WATERMARK TO MERGE_PARTS
    interface_1, interface_2 = node_API.connect_nodes(vintage_node_ip, merge_parts_node_ip, '6', '7', '51820',
                                                      '24adna',
                                                      '10.0.6.')
    save_connection('6', '7', interface_1, interface_2, '10.0.6.x')

    node_API.launch_checker_daemon(merge_parts_node_ip)
    node_API.setup_consumer(VINTAGE_NODE_LO, merge_parts_node_ip, filter_types.MERGE_PARTS)
    node_API.start_socket(merge_parts_node_ip, controller_ip)
    node_API.run_merge_parts_service(merge_parts_node_ip)


def compute_remaining_processing_time(q_size, node_filter):
    pass


def get_least_used_node(bottleneck_id):

    for node in nodes:
        try:
            skips = freshly_assigned_nodes[node.id]
        except KeyError:
            skips = None

        if node.id != '1' and node.id != '2' and node.id != '3' and node.id != '6' and node.id != '4' and node.id != '5' and node.id != '7':
            # check the queue size and see if its empty
            q_size = node_API.get_cumulative_queue_size(node.ip)
            if q_size == 0 and not node_already_connected_to(node.id, bottleneck_id) and node_API.ffmpeg_has_not_ran_in_x_seconds(node.ip, 30):
                return node
            elif skips is not None:
                freshly_assigned_nodes[node.id] -= 1

    return None


def node_already_connected_to(this_node_id, connected_to_id):
    this_node = get_node(this_node_id)
    this_node_connections = this_node.connections
    for connection in this_node_connections:
        if connection.with_node_id == connected_to_id:
            return True
    return False


def setup_connections(node1_ip, node1_id, node1_lo, node1_next_port, node1_function, next_subnet, node2_ip, node2_id,
                      node2_lo, controller_ip, yes_subseq):
    bottleneck_interface, idle_interface = node_API.connect_nodes(node1_ip, node2_ip,
                                                                  node1_id, node2_id,
                                                                  node1_next_port, '24adna',
                                                                  next_subnet.replace('x', ''))

    set_last_used_port(node1_id, node1_next_port)
    save_connection(node1_id, node2_id, bottleneck_interface, idle_interface, next_subnet)

    idle_node_function = get_subsequent_function(node1_function)

    get_node(node2_id).function = idle_node_function

    # node_creator.setup_consumer(node1_lo, node2_ip, idle_node_function)
    node_API.launch_checker_daemon(node2_ip)
    #node_API.launch_ffmpeg_last_run_check(node2_ip)

    node_API.setup_consumer(queue_ip=node1_lo, vm_ip=node2_ip,
                            task_type=idle_node_function)  # idle node consumes from bottleneck

    node_API.setup_producer(vm_ip=node2_ip, loopback_addr=node2_lo)
    node_API.start_socket(node2_ip, controller_ip)

    if yes_subseq:
        subs_nodes = get_subsequent_nodes_of(idle_node_function, node2_id, idle_node_function)
        time.sleep(3)
        for node in subs_nodes:
            # no actual wireguard setup for sub nodes?
            print(
                f'==================================================================================================== Consumer of idle node {node2_id} will be {node.id} with ip {node.ip} and func {node.function}')
            # setup wireguard for consumer node
            print('SETUP WIREGUARD FOR CONSUMER NODE')

            node_2 = get_node(node2_id)
            next_port_idle = str(int(node_2.las_used_port) + 1)
            node_subnets = get_subnets_of_node(node.id)
            node_subnets.append(get_subnets_of_node(node2_id))  # make sure no subnet conflicts between the 2 nodes
            next_node_subnet = get_next_subnet(node_subnets)

            interface1, interfac2 = node_API.connect_nodes(producer_ip=node2_ip, consumer_ip=node.ip, producer_id=node2_id,
                                                           consumer_id=node.id, port_producer=next_port_idle,
                                                           vm_passwd='24adna', subnet=next_node_subnet.replace('x', ''),
                                                           lo_flag=False)
            save_connection(id_1=node2_id, id_2=node.id, interface_name_1=interface1, interface_name_2=interfac2,
                            subnet=next_node_subnet)

            node_API.launch_checker_daemon(node.ip)
            node_API.setup_consumer(queue_ip=node2_lo, vm_ip=node.ip, task_type=node.function)


def assign_node(idle_node, bottleneck_id, bottleneck_ip, bottleneck_function, bottleneck_lo, yes_subseq):
    idle_node_id = idle_node.id
    idle_node_ip = idle_node.ip
    idle_node_lo = '172.16.0.' + idle_node_id
    next_port_producer = str(int(get_node_last_used_port(bottleneck_id)) + 1)

    print(f'@@@@@@@@@@@@@@@@@ Idle node {idle_node_id} connecting to bottleneck node: {bottleneck_id}')

    current_subnets = get_subnets_of_node(bottleneck_id)
    next_subnet = get_next_subnet(current_subnets)
    print(f'++++++++++++++ Next subnet of node node {idle_node_id} will be {next_subnet}')
    setup_connections(bottleneck_ip, bottleneck_id, bottleneck_lo, next_port_producer, bottleneck_function, next_subnet,
                      idle_node_ip, idle_node_id, idle_node_lo, controller_ip, yes_subseq)


def get_number_of_consumers(of_node):
    nr_consumers = 0
    for connection in of_node.connections:
        if not connection.is_consumer:  # i am producer
            nr_consumers += 1
    return nr_consumers


def compute_remaining_work(of_bottleneck_node, nr_elements):
    # TO DO: Fill correct average for each filter
    # Only works for BLUR, STABILIZE
    remaining_work = -1
    nr_of_consumers = get_number_of_consumers(of_bottleneck_node)
    if nr_of_consumers != 0:
        if of_bottleneck_node.function == filter_types.SPLIT:
            remaining_work = (12 * nr_elements) / nr_of_consumers
        elif of_bottleneck_node.function == filter_types.BLUR:
            remaining_work = (12 * nr_elements) / nr_of_consumers
        elif of_bottleneck_node.function == filter_types.STABILIZE:
            remaining_work = (12 * nr_elements) / nr_of_consumers
        elif of_bottleneck_node.function == filter_types.ADD_WATERMARK:
            remaining_work = (12 * nr_elements) / nr_of_consumers
        elif of_bottleneck_node.function == filter_types.REVERSE:
            remaining_work = (12 * nr_elements) / nr_of_consumers
        elif of_bottleneck_node.function == filter_types.VINTAGE:
            remaining_work = (12 * nr_elements) / nr_of_consumers

    print(
        f'================================================================================================== REMAINING WORK: {remaining_work} seconds with {nr_of_consumers} consumers')
    return remaining_work


def scale_up_node_with_least_used_node(bottleneck_node, least_used_node):
    print(
        'OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO BEGIN OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO')

    print(
        f'------------------------------------------- Resetting connections with least used node {least_used_node.id} ')
    bottleneck_id = bottleneck_node.id
    bottleneck_ip = bottleneck_node.ip
    bottleneck_lo = '172.16.0.' + bottleneck_id
    bottleneck_function = bottleneck_node.function

    print(f'FREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEESHLY ASSIGNED {bottleneck_id} {FRESHLY_SCALED_NODES_SKIP}')
    node_API.add_FIN_file(least_used_node.ip)
    # CONNECT THE NODE TO THE IDLE NODE
    # Remove connections of IDLE node
    for connection in least_used_node.connections:
        # loop through all connections
        least_used_node_connection_wth_id = connection.with_node_id
        least_used_node_connection_wth_node = get_node(least_used_node_connection_wth_id)
        conn_index = 0

        for c in least_used_node_connection_wth_node.connections:
            # loop though connections of node where we connected
            if c.with_node_id == least_used_node.id:
                # Find idle node in node connections and remove that connection
                print(
                    f'----------------------------- Removing connection of {least_used_node_connection_wth_id} with least used node {least_used_node.id}')
                # delete connection with this
                least_used_node_connection_wth_node_connection = []
                element = least_used_node_connection_wth_node.connections.pop(
                    conn_index)  # a connection element
                least_used_node_connection_wth_node_connection.append(element)
                # SEND FIN packet to itself
                node_API.kill_consumer_process(least_used_node_connection_wth_node.ip)

                node_API.bring_down_all_wireguard_interfaces(
                    least_used_node_connection_wth_node.ip,
                    least_used_node_connection_wth_node.connections)

                time.sleep(4)
                node_API.stop_ospfd_and_zebra(least_used_node_connection_wth_node.ip)

                node_API.delete_wireguard_interfaces(least_used_node_connection_wth_node.ip,
                                                     least_used_node_connection_wth_node_connection,
                                                     least_used_node_connection_wth_node.id)

                time.sleep(4)
                # KILL all processes
                least_used_node_lo = '172.16.0.' + least_used_node.id
                node_API.delete_checker_daemon_config_line(
                    least_used_node_connection_wth_node.ip, least_used_node_lo, least_used_node_connection_wth_id )

                node_API.bring_up_all_wireguard_interfaces(
                    least_used_node_connection_wth_node.ip,
                    least_used_node_connection_wth_node.connections)

                time.sleep(4)
                node_API.start_ospfd_and_zebra(least_used_node_connection_wth_node.ip)
                time.sleep(4)
                node_API.launch_checker_daemon(least_used_node_connection_wth_node.ip)
                time.sleep(4)
                # restore producer process
                if least_used_node_connection_wth_node.id != LAST_PIPELINE_NODE:
                    node_API.setup_producer(least_used_node_connection_wth_node.ip,
                                            '172.16.0.' + least_used_node_connection_wth_node.id)
                else:
                    node_API.run_merge_parts_service(least_used_node_connection_wth_node.ip)
                # node 3 needs to consume node 2 after reboot
                # restore connections lost upon reboot
                for c2 in least_used_node_connection_wth_node.connections:
                    # restore consumer process (3 FROM 2)
                    if c2.is_consumer and c2.with_node_id != least_used_node.id:
                        # restore consumer processes after reboot
                        node_API.launch_checker_daemon(least_used_node_connection_wth_node.ip)
                        node_API.setup_consumer('172.16.0.' + c2.with_node_id,
                                                least_used_node_connection_wth_node.ip,
                                                least_used_node_connection_wth_node.function)
                    # restore consumer process (6 -> 3)
            conn_index += 1

    # TO DO: also delete wireguard interfaces
    # HEEEELPER NODE RESET
    print(f'------------------------------------------- Helper node reset {least_used_node.id}')
    node_API.kill_consumer_process(least_used_node.ip)
    # time.sleep(5)
    # VMware_API.turn_off_vm(least_used_node.id)
    # least_used_node.ip = VMware_API.turn_on_vm_and_get_ip(least_used_node.id) # update the ip after reboot just to be sure
    # NODE 4,5 SETUP
    node_API.bring_down_all_wireguard_interfaces(least_used_node.ip,
                                                 least_used_node.connections)

    # time.sleep(100)
    node_API.stop_ospfd_and_zebra(least_used_node.ip)
    node_API.delete_wireguard_interfaces(least_used_node.ip, least_used_node.connections,
                                         least_used_node.id)
    node_API.delete_checker_daemon_config_line(least_used_node.ip)
    node_API.launch_checker_daemon(least_used_node.ip)

    least_used_node.connections = []
    print(
        f'--------------------------------------------------------------------------------- Reassign least used node {least_used_node.id} to {bottleneck_id}')
    assign_node(least_used_node, bottleneck_id, bottleneck_ip, bottleneck_function,
                bottleneck_lo)
    freshly_assigned_nodes[least_used_node.id] = FRESHLY_ASSINED_NODE_SKIP_COUNT
    print('OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO END OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO')


def get_next_producer_folder_name(node_obj):
    current_queue_index = node_obj.current_queue_index
    next_queue_index = current_queue_index + 1
    return filter_types.SECOND_PRODUCER_FOLDER_NAME + str(next_queue_index)


def get_current_queue_name(node_obj):
    queue_name = filter_types.DEFAULT_QUEUE_NAME
    current_queue_index = node_obj.current_queue_index
    if current_queue_index == 1:
        return queue_name
    return queue_name + str(current_queue_index)


def get_next_self_queue(node_obj):
    current_queue_index = node_obj.current_queue_index
    next_queue_index = current_queue_index + 1
    queue_name = 'hello' + str(next_queue_index)
    return queue_name


def get_last_queue_size(node_obj):
    q_name = get_current_queue_name(node_obj)
    q_size = node_API.get_q_size(node_obj.ip, q_name)
    return q_size


def get_function_level(func):
    if func == filter_types.SPLIT:
        return 1
    if func == filter_types.BLUR:
        return 2
    if func == filter_types.STABILIZE:
        return 3
    if func == filter_types.ADD_WATERMARK:
        return 4
    if func == filter_types.REVERSE:
        return 5
    if func == filter_types.VINTAGE:
        return 6
    if func == filter_types.MERGE_PARTS:
        return 7
    return -1


def all_above_queues_have_been_consumed(node_obj):
    node_obj_level = get_function_level(node_obj.function)
    sum = 0
    for node in nodes:
        if node.id in pipeline_nodes:
            crt_node_level = get_function_level(node.function)
            if crt_node_level < node_obj_level:
                q_size = node_API.get_q_size(node.ip)
                sum += q_size
    if sum == 0:
        return True
    return False


def self_scale(bottleneck_lo, bottleneck_ip, bottleneck_id, bottleneck_node_obj):
    # consume its own queue to prevent idle CPU
    # ISSUE: hello2 sub_function is not the same as hello3 sub_function
    # ISSUE: which queue size matters for self sclaing?
    bottleneck_function = bottleneck_node_obj.last_function
    if bottleneck_node_obj.last_function == filter_types.IDLE:
        bottleneck_function = bottleneck_node_obj.function

    # node_API.setup_consumer()
    current_queue_name = get_current_queue_name(bottleneck_node_obj) #hello

    next_queue_name = get_next_self_queue(bottleneck_node_obj) #hello2

    subsequent_function = get_subsequent_function(bottleneck_function)

    next_producer_folder_name = get_next_producer_folder_name(bottleneck_node_obj)

    # self consume and put output in second producer folder

    node_API.create_next_producer_folder(bottleneck_ip, next_producer_folder_name)
    node_API.setup_consumer(bottleneck_lo, bottleneck_ip, subsequent_function,
                            next_producer_folder_name, current_queue_name) # save output from self consume to new folder
    # setup second queue
    node_API.setup_producer(bottleneck_ip, bottleneck_lo, next_queue_name, next_producer_folder_name)
    # setup consumer to consume from second queue

    subsequenct_nodes = get_subsequent_nodes_of(subsequent_function, bottleneck_id, subsequent_function)

    for subsequent_node in subsequenct_nodes:
        if subsequent_node.id in pipeline_nodes:
            node_API.setup_consumer(bottleneck_lo, subsequent_node.ip, subsequent_node.function,
                                    next_queue_name)
            # Save connection to object
            for subsequent_node_connection in subsequent_node.connections:
                if subsequent_node_connection.with_node_id == bottleneck_id:
                    subsequent_node_connection.q_names.append(next_queue_name)
                    break
    bottleneck_node_obj.current_queue_index += 1
    bottleneck_node_obj.last_function = subsequent_function


def get_all_q_names(q_list):
    msg = ' '
    for q_name in q_list:
        msg += f'{q_name} AND '
    return msg


def print_node_connected_to(node_obj):
    msg = f'(INFOOOO) Node_id {node_obj.id}  '
    for node_connection in node_obj.connections:
        msg += f'CONSUMES FROM {node_connection.with_node_id} : {node_connection.is_consumer}  '
        if node_connection.is_consumer:
            msg += ' QUEUE NAMES ' + get_all_q_names(node_connection.q_names)
        else:
            msg += ' | '
    print(msg)


def assign_all_idle_nodes_to_1():
    print('ASSIGNING ALL IDLE NODES TO FIRST PIPELINE NODE')
    # assign all idle nodes to first pipeline node
    crt_idle_node = get_idle_node()
    first_pipe_node_obj = get_node('1')
    while crt_idle_node:
        print(f'ASSSSSSSSSIGN IDLE NODE {crt_idle_node.id}')
        assign_node(crt_idle_node, first_pipe_node_obj.id, first_pipe_node_obj.ip, first_pipe_node_obj.function,
                    '172.16.0.1')
        crt_idle_node = get_idle_node()


def create_parallel_pipeline():
    second_filter_node_obj = get_idle_node()
    bottleneck_node_obj = get_node('1')
    assign_node(second_filter_node_obj, '1', bottleneck_node_obj.ip, bottleneck_node_obj.function, '172.16.0.1', False)
    node_API.launch_receive_files_process(vm_ip=second_filter_node_obj.ip, host_ip=f'172.16.0.{second_filter_node_obj.id}')
    # node_API.setup_producer(vm_ip=second_filter_node_obj.ip, loopback_addr=f'172.16.0.{second_filter_node_obj.id}',
    #                         queue_name='first', folder_name='distributed_files')
    node_API.setup_consumer(queue_ip=f'172.16.0.{second_filter_node_obj.id}', vm_ip=second_filter_node_obj.ip,
                            task_type=filter_types.BLUR, third_argument='first')

    third_filter_node_obj = get_idle_node()
    bottleneck_node_obj = get_node(second_filter_node_obj.id)
    assign_node(third_filter_node_obj, bottleneck_node_obj.id, bottleneck_node_obj.ip, bottleneck_node_obj.function, '172.16.0.'+bottleneck_node_obj.id, False)

    forth_filter_node_obj = get_idle_node()
    bottleneck_node_obj = get_node(third_filter_node_obj.id)
    assign_node(forth_filter_node_obj, bottleneck_node_obj.id, bottleneck_node_obj.ip, bottleneck_node_obj.function,
                '172.16.0.' + bottleneck_node_obj.id, False)

    fifth_filter_node_obj = get_idle_node()
    bottleneck_node_obj = get_node(forth_filter_node_obj.id)
    assign_node(fifth_filter_node_obj, bottleneck_node_obj.id, bottleneck_node_obj.ip, bottleneck_node_obj.function,
                '172.16.0.' + bottleneck_node_obj.id, False)

    sixth_filter_node_obj = get_idle_node()
    bottleneck_node_obj = get_node(fifth_filter_node_obj.id)
    assign_node(sixth_filter_node_obj, bottleneck_node_obj.id, bottleneck_node_obj.ip, bottleneck_node_obj.function,
                '172.16.0.' + bottleneck_node_obj.id, True)
    return second_filter_node_obj


def launch_producer_process():
    for node in nodes:
        if node.id == '2' or node.id == '10' or node.id == '15':
            print(f'Launching producer process for node {node.id}')
            node_API.setup_producer(vm_ip=node.ip, loopback_addr=f'172.16.0.{node.id}', queue_name='first',
                                    folder_name='distributed_files')

def start_parallel_pipelines():
    first_blur_pipeline_node = create_parallel_pipeline()
    second_blur_pipeline_node = create_parallel_pipeline()
    return first_blur_pipeline_node, second_blur_pipeline_node


if __name__ == '__main__':
    global controller_ip
    controller_ip = sys.argv[1]
    start_initial_pipeline()
    first_blur_pipeline_node, second_blur_pipeline_node = start_parallel_pipelines()
    node_2_default_pipeline = get_node('2')
    node_1 = get_node('2')
    print(f'first_blur_pipeline_node {first_blur_pipeline_node.id} second_blur_pipeline_node {second_blur_pipeline_node.id}')
    unit_test_cloud_parallel_pipeline.distribute_files()
    time.sleep(20)
    launch_producer_process()
    unit_test_cloud_parallel_pipeline.start()
    # node_API.launch_distribute_files(vm_ip=node_1.ip, nr_of_nodes='3',
    #                                  nodes_ips=['172.16.0.2', f'172.16.0.{first_blur_pipeline_node.id}', f'172.16.0.{second_blur_pipeline_node.id}'])
